<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css' media="all">
	<link href="{{ URL::to('css/bootstrap.min.css')}}" rel="stylesheet"  media="all"><!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="{{ URL::to('css/reset.css') }}" media="all"> <!-- CSS reset -->
	<link rel="stylesheet" href="{{ URL::to('css/portal.css') }}" media="all"> <!-- Resource style -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css" media="all">
	<!-- Sweet Alert Style -->
	<link rel="stylesheet" href="{{URL::to('plugins/sweet-alert/sweetalert.css')}}" media="all">
	<!-- daterange picker -->
	<link rel="stylesheet" href="{{URL::to('plugins/daterangepicker/daterangepicker.css')}}" media="all">
	<!-- daterange picker -->
	<link rel="stylesheet" href="{{URL::to('plugins/datepicker/datepicker3.css')}}" media="all">

	<script src="{{ URL::to('js/modernizr.min.js') }}"></script> <!-- Modernizr -->

	<title>iTOOhL | Portal</title>
</head>
<body>	
	<section id="google-map">		
		<div id="google-container"></div>
		<div id="circle" data-toggle="tooltip" data-placement="left" title="Show marker in circle"></div>
		<div id="traffic" data-toggle="tooltip" data-placement="left" title="Traffic Layer"></div>
		<div id="zoom-in" data-toggle="tooltip" data-placement="left" title="Zoom in"></div>
		<div id="zoom-out" data-toggle="tooltip" data-placement="left" title="Zoom out"></div>
		<div id="map-view">Map</div>
		<div id="list-view">List</div>
	</section>

	<div class="filter filter-is-visible">
		<div class="filter-title">
			<a href="{{ URL::route('user-dashboard') }}"><img src="{{ URL::to('img/logo-white.png') }}" height="50" width="50"></a>
		</div>
		<form>
			<div class="filter-block">
				<div class="filter-content filter-search">
					<input type="search" placeholder="Search..." name="search"  id="search">
				</div> <!-- filter-content -->
			</div> <!-- filter-block -->
			
			<div class="filter-block">
				<div class="switcher">
					<p class="fieldset">
						<input type="radio" name="switcher" id="basic" checked>
						<label for="basic">Basic</label>
						<input type="radio" name="switcher" class="demographic" id="demographic">
						<label for="demographic">Demographic</label>
						<span class="switch"></span>
					</p>
				</div> <!-- .switcher -->
			</div>
			
			<div class="filter-scroll">

				<div class="basic-filter">
					<div class="filter-block">
						<h4>Industries</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $industries as $industry )
							<li>
								<input class="filter" data-filter=".{{ $industry->name }}" type="checkbox"  id="{{ $industry->name }}" name="{{ $industry->name }}" value="{{ $industry->name }}">
								<label class="checkbox-label" for="{{ $industry->name }}">{{ $industry->name }}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div>		

					<div class="filter-block">
						<h4>Format</h4>

						<ul class="filter-content filters list" style="display: none;">

							@foreach( $formats as $format) 
							<li>
								<input class="filter" data-filter=".{{ $format->name }}" type="checkbox" id="{{ $format->name }}" name="{{ $format->name }}" value="{{ $format->name }}" >
								<label class="checkbox-label" for="{{ $format->name }}">{{ $format->name }}</label>
							</li>
							@endforeach


							<!--<li class="block" style="margin-left: 1em;">
								<h4>Ambient</h4>

								<ul class="filter-content filters list" style="display: none;">
									@foreach( $formats->slice(25, 3) as $format) 
									<li>
										<input class="filter" data-filter=".{{ $format->name }}" type="checkbox" id="{{ $format->name }}" name="{{ $format->name }}" value="{{ $format->name }}" >
										<label class="checkbox-label" for="{{ $format->name }}">{{ $format->name }}</label>
									</li>
									@endforeach
								</ul>
							</li>-->

							<!--<li class="block" style="margin-left: 1em;">
								<h4>Transit Ads</h4>

								<ul class="filter-content filters list" style="display: none;">
									@foreach( $formats->slice(29, 7) as $format) 
									<li>
										<input class="filter" data-filter=".{{ $format->name }}" type="checkbox" id="{{ $format->name }}" name="{{ $format->name }}" value="{{ $format->name }}" >
										<label class="checkbox-label" for="{{ $format->name }}">{{ $format->name }}</label>
									</li>
									@endforeach
								</ul>
							</li>-->

						</ul> <!-- filter-content -->
					</div> <!-- filter-block -->


					<div class="filter-block">
						<h4>Availability</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $availabilities as $availability )
							<li>
								<input class="filter" data-filter=".{{$availability->name}}" type="checkbox" name="{{$availability->name}}" id="{{$availability->name}}" value="{{$availability->name}}" >
								<label class="checkbox-label" for="{{$availability->name}}">{{$availability->name}}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div> <!-- filter-block -->


					<div class="filter-block">
						<h4>Location</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $cities->slice(0, 4) as $city )
							<li>
								<input class="filter" data-filter=".{{$city->name}}" type="checkbox" name="{{$city->name}}" id="{{$city->name}}" value="{{$city->name}}" >
								<label class="checkbox-label" for="{{$city->name}}">{{$city->name}}</label>
							</li>
							@endforeach

							<li class="block" style="margin-left: 1em;">
								<h4>City</h4>

								<ul class="filter-content filters list" style="display: none;">
									@foreach( $cities->slice(4, 144) as $city) 
									<li>
										<input class="filter" data-filter=".{{ $city->name }}" type="checkbox" id="{{ $city->name }}" name="{{ $city->name }}" value="{{ $city->name }}" >
										<label class="checkbox-label" for="{{ $city->name }}">{{ $city->name }}</label>
									</li>
									@endforeach
								</ul>
							</li>

						</ul><!-- filter-content -->
					</div> <!-- filter-block -->
				</div><!-- basic-filter -->

				<div class="demographic-filter is-hidden">
					
					<div class="filter-block">
						<h4>Industries</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $industries as $industry )
							<li>
								<input class="filter" data-filter=".{{ $industry->name }}" type="checkbox"  id="{{ $industry->name }}2" name="{{ $industry->name }}" value="{{ $industry->name }}">
								<label class="checkbox-label" for="{{ $industry->name }}2">{{ $industry->name }}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div>	

					<div class="filter-block">
						<h4>Age</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $ages as $age )
							<li>
								<input class="filter" data-filter=".{{ $age->name }}" type="checkbox"  id="{{ $age->name }}" name="{{ $age->name }}" value="{{ $age->name }}">
								<label class="checkbox-label" for="{{ $age->name }}">{{ $age->name }}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div>

					<div class="filter-block">
						<h4>Profile</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $profiles as $profile )
							<li>
								<input class="filter" data-filter=".{{ $profile->name }}" type="checkbox"  id="{{ $profile->name }}" name="{{ $profile->name }}" value="{{ $profile->name }}">
								<label class="checkbox-label" for="{{ $profile->name }}">{{ $profile->name }}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div>
					
					<div class="filter-block">
						<h4>Gender</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $genders as $gender )
							<li>
								<input class="filter" data-filter=".{{ $gender->name }}" type="checkbox"  id="{{ $gender->name }}" name="{{ $gender->name }}" value="{{ $gender->name }}">
								<label class="checkbox-label" for="{{ $gender->name }}">{{ $gender->name }}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div>

					<div class="filter-block">
						<h4>Economic Class</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $classes as $class )
							<li>
								<input class="filter" data-filter=".{{ $class->name }}" type="checkbox"  id="{{ $class->name }}" name="{{ $class->name }}" value="{{ $class->name }}">
								<label class="checkbox-label" for="{{ $class->name }}">{{ $class->name }}</label>
							</li>
							@endforeach
						</ul><!-- filter-content -->
					</div>

					<div class="filter-block">
						<h4>Location</h4>

						<ul class="filter-content filters list" style="display: none;">
							@foreach( $cities->slice(0, 4) as $city )
							<li>
								<input class="filter" data-filter=".{{$city->name}}" type="checkbox" name="{{$city->name}}" id="{{$city->name}}2" value="{{$city->name}}" >
								<label class="checkbox-label" for="{{$city->name}}2">{{$city->name}}</label>
							</li>
							@endforeach

							<li class="block" style="margin-left: 1em;">
								<h4>City</h4>

								<ul class="filter-content filters list" style="display: none;">
									@foreach( $cities->slice(4, 144) as $city) 
									<li>
										<input class="filter" data-filter=".{{ $city->name }}" type="checkbox" id="{{ $city->name }}2" name="{{ $city->name }}" value="{{ $city->name }}" >
										<label class="checkbox-label" for="{{ $city->name }}2">{{ $city->name }}</label>
									</li>
									@endforeach
								</ul>
							</li>

						</ul><!-- filter-content -->
					</div> <!-- filter-block -->
		
				</div><!-- demographic-filter -->
			</div><!-- slim scroll -->
		</form>


		
		<div class="col-md-12 side-bar-container">
			<div class="side-bar-box">
				<ul id="markers">
					
				</ul>
			</div>
		</div>

	</div> <!-- filter -->

	

	<!-- Inventory Single Item -->

	<div class="inventory">
		<div class="row">
			<div class="col-md-12">
				<div class="inventory-box">
					<div class="inventory-header">
						<a href="#0" id="close" class="close">
							<i class="fa fa-chevron-left"></i
								><span> Back</span>
							</a>
							<center><p class="print-only">iTOOhL</p></center>
						</div>
						<div class="inventory-body">
							<img class="inventory-image" src="img/inventory_image_default.jpg">
							<video  class="inventory-image" autoplay="autoplay" controls="false" loop>
								<source src="" type="video/mp4" />
							</video>
						</div>
						<div class="inventory-footer">
							<a href="#" id="name"></a>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12" style="
    overflow: auto;
    max-height: 250px;
    height: 100%;">
					<div class="inventory-content">
					    <li><b>Format:</b> <span id="format"></span></li>
					    <li><b>Location:</b> </li>
						<li><b>Size (H x W):</b> <span id="size"></span></li>
						<li><b>Availability:</b> <span id="availability"></li>
						<li><b>Product:</b> <span id="product"></li>
						<li><b>Category:</b> <span id="product"></li>
						<li><b>Industry:</b> <span id="industry"></span></li>
						<li><b>Traffic Count:</b> <span></span></li>
						<li><b>iTOOhL Rating:</b> <span></span></li>
						<li><b>Published Rate:</b> &#x20B1;<span id="rates"></span></li>
					</div>
					<div class="inventory-content-footer">
						<a href="#" data-toggle="modal" data-target="#request-quotes">
							<li>
								<i class="fa fa-wechat"></i>
								<span>Request a Quote</span>	
							</li>
						</a>
						<a href="#" onclick="myFunction()">
							<li>
								<i  class="fa fa-print"></i>
								<span>Print</span>	
							</li>
						</a>
						<a href="" id="bookmark">
							<li>
								<i  class="fa fa-bookmark-o"></i>
								<span>Bookmark</span>	
							</li>
						</a>
					</div>
				</div>
			</div>


		</div> <!-- filter -->


		<!-- Modal -->
		@include('includes.quotes-modal')


<script src="{{ URL::to('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::to('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
	<!--<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8hnRJwlyh4zeJ_AjxFwkAHsxBtAG0Kcs&libraries=visualization,places"></script>
<script src="{{ URL::to('js/bootstrap.min.js') }}"></script> <!-- Bootstrap Core JavaScript -->
<script src="{{ URL::to('js/portal.min.js')}}"></script> <!-- Resource jQuery -->
@include ('includes.map')
<!-- Sweet Alert -->
<script src="{{ URL::to('plugins/sweet-alert/sweetalert.min.js') }}"></script>
@include('sweet::alert')
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::to('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{ URL::to('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Page script -->
<!-- Slimscroll -->
<script src="{{ URL::to('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>	
<script>
	$(function () {


	    //Date range picker
	    $('#reservation').daterangepicker();


		//Date picker
		$('#material_changes').datepicker({
			autoclose: true
		});


	});

</script>

<!--portal print pdf -->
<script>
	
	function myFunction() {
		window.print();
	}

  </script>

</script>

</body>
</html>