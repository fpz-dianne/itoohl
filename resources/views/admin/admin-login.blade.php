<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iTOOhL | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('css/admin.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('plugins/iCheck/flat/blue.css') }}">
    <!-- Sweet Alert Style -->
    <link rel="stylesheet" href="{{URL::to('plugins/sweet-alert/sweetalert.css')}}" media="all">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ route('home') }}"><b><img src="{{ URL::to('img/logo.png')}}"></b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <h2 class="login-box-msg text-white">Sign in to start your session</h2>
            <form id="from1" action="{{ route('admin-login') }}" method="post">
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="margin-bottom: 1em;">
                        <a href="{{ URL::route('getstarted') }}" class="text-white pull-left">Register</a>
                        <a href="#" class="text-white pull-right">I forgot my password</a><br>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat popup-validation">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="{{ URL::to('js/jquery.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ URL::to('js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ URL::to('plugins/iCheck/icheck.min.js')}}"></script>
    <!-- Sweet Alert -->
    <script src="{{ URL::to('plugins/sweet-alert/sweetalert.min.js') }}"></script>
    @include('sweet::alert')

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_flat-blue',
          radioClass: 'iradio_flat-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>

    <script>
    @if ($errors->any())
    	sweetAlert({
    		title: "Error!",
    		text: "@foreach($errors->all() as $error) {{ $error }}\n @endforeach",
    		type: "error",
    		confirmButtonColor: "#DD6B55"
    	});
    @endif
    </script>
</body>
</html>
