@extends('layouts.master')

@section('title')
iTOOhL | Get Started
@endsection

@section('content')

<div class="row" style="margin: 8em 0 4em;">
	<section class="col-md-3 clouds-container">
		<img src="{{ URL::to('img/clouds.png') }}" alt="">
	</section>
	<section class="col-md-6 text-center get-started-section">
		<h3>
			Please 
			<span class="text-pink" style="font-family: 'HelveticaNeue-Medium'; font-size: 30px; ">Login or Register</span> to continue
		</h3>
		<p>
			by creating an account, you will be able to
		</p>
		<p>
			access the portal
		</p>
	</section>
	<section class="col-md-3 airplane-container"><img src="{{ URL::to('img/airplane.png') }}" alt=""></section>
</div><!--End of row-->

<div class="row get-started-container" style="margin: 4em 2em;">
	<section class="col-md-5 col-sm-12">
		<div class="login box box-default">
			<div class="box-header bg-gray">
				<h3>Login to our site</h3>
				<p class="pull-left">Enter your username and password to login</p>
				<span class="text-pink"><i class="fa fa-lock fa-4x pull-right list-inline"></i></span>
			</div>
			<div class="box-body bg-pink">
				<form action="{{ route('signin') }}" method="post">
					<div class="form-group">
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					</div>
					<div class="form-group">
						<input type="submit" class="login-btn form-control bg-yellow" value="Login">
					</div>
					<input type="hidden" name="_token" value="{{ Session::token() }}">
					{{ csrf_field() }}
				</form>
			</div>
			<!--<div class="box-footer">
				<p class="text-center">...or login with</p>
				<ul class="login-with list-inline col-md-offset-1">
					<li><a href="#"><button class="btn btn-default"><i class="text-navy fa fa-facebook"></i> Facebook</button></a></li>
					<li><a href="#"><button class="btn btn-default"><i class="text-aqua fa fa-twitter"></i> Twitter</button></a></li>
					<li><a href="#"><button class="btn btn-default"><i class="text-gray fa fa-google-plus"></i> Google+</button></a></li>
					
				</ul>
			</div>-->
		</div>
	</section>

	<section class="col-md-5 col-sm-12">
		<div class="register box box-default">
			<div class="box-header bg-gray">
				<h3>Register now</h3>
				<p class="pull-left">Fill in the form below to get instant access</p>
				<span class="text-pink"><i class="fa fa-pencil fa-4x pull-right list-inline"></i></span>
			</div>
			<div class="box-body bg-pink">
				<form action="{{ route('signup') }}" method="post">
					<div class="form-group">
					<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
					</div>
					<div class="form-group">
						<input type="email" class="form-control" name="email" id="email" placeholder="Email">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					</div>
					<div class="form-group">
						<input type="submit" class="register-btn form-control bg-yellow" value="Register">
					</div>
					{{ csrf_field() }}
				</form>
			</div>
		</div>
	</section>
</div><!--end of row-->

<div class="row">
	<section class="col-md-12 transport-container">
		<img src="{{ URL::to('img/transport.png') }}" alt="">
	</section>
</div><!--end of row-->

@endsection

@section('script')
<script>
@if ($errors->any())
	sweetAlert({
		title: "Error!",
		text: "@foreach($errors->all() as $error) {{ $error }}\n @endforeach",
		type: "error",
		confirmButtonColor: "#DD6B55"
	});
@endif
</script>
@endsection
