@extends('layouts.master')

@section('title')
Itoohl | Single News
@endsection

@section('content')
<section id="single-news" class="col-md-12">
	<div class="row">
		
		@include('includes.news-left-sidebar')

		<div class="col-md-7">
			<div class="single-news-container">
				<h1 class="single-news-title">Connect with pedestrians and vehicular traffic alike</h1>
				<p>
					<a href="javascript::void(0)">Tech by:</a> Robert Cruda
				</p>
				<img class="img-responsive" src="{{ URL::to('img/news-item-1.jpg') }}" alt="">
				<p class="single-news-content">
					Staples Center Transit shelter in Los Angeles. Let's Connect! Street Furniture enable advertising that connects with consumers on a intimate, face-to-face level Options such as becnh advertising and bus shelter advertising impact to a mass vehicular and pedestrian audience. Whether providing broad-based coverage in many markets or targeted to a single neighborhood, Street Furniture advertising is as perfect for high-end fashion entertainment as it is for packaged goods.
				</p>
			</div>
		</div>

		@include('includes.news-right-sidebar')
	</div>
</section>
@endsection

@section('script')
<!-- Itoohl App -->
<script src="{{ URL::to('js/app.min.js')}} "></script>
@endsection