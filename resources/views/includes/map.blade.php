<script type = "text/javascript">
	var latitude = 14.5501849903,
	    longitude = 121.0319780001,
	    mapZoom = 11;

	// Google map custom marker icon
	// .png fallback for IE11
	var isIE11 = navigator.userAgent.toLowerCase().indexOf('trident') > -1;
	var markerUrl = (isIE11) ? 'img/icon-locationp.png' : 'img/icon-location.png';
	var markerDarkUrl = (isIE11) ? 'img/icon-location.png' : 'img/icon-location.png';

	// Define the basic color of your map, plus a value for saturation and brightness
	var mainColor = '#2d313f',
	    saturationValue = -20,
	    brightnessValue = 5;

	// We define here the style of the map
	var style = [

	];

	// Set google map options
	var mapOptions = {
	    center: new google.maps.LatLng(latitude, longitude),
	    zoom: mapZoom,
	    panControl: false,
	    zoomControl: false,
	    mapTypeControl: false,
	    mapTypeControlOptions: {
	        position: google.maps.ControlPosition.TOP_RIGHT
	    },
	    streetViewControl: false,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    scrollwheel: true,
	    styles: style,
	}

	// Inizialize the map
	var map = new google.maps.Map(document.getElementById('google-container'), mapOptions);

	/*
	// Add a custom marker to the map
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(latitude, longitude),
		map: map,
		visible: false,
		icon: markerUrl
	});
	*/

	google.maps.event.addListener(map, 'click', function(event) {
	    addMarker(event.latLng, map);
	});

	// Add custom buttons for the tools on the map
	function CustomMapview(controlviewDiv, map) {
	    // Graph the mapview elements from the DOM and insert them in the map
	    controlUiMapView = document.getElementById('map-view'),
	    controlUiListView = document.getElementById('list-view');
	    controlviewDiv.appendChild(controlUiMapView);
	    controlviewDiv.appendChild(controlUiListView);

	    google.maps.event.addDomListener(controlUiMapView, 'click', function() {
	        window.location.href = "{{ URL::route('portal') }}";
	    });

	    google.maps.event.addDomListener(controlUiListView, 'click', function() {
	        window.location.href = "{{ URL::route('portal-list') }}";
	    });
	}

	var viewControlDiv = document.createElement('div');
	viewControlDiv.style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
	viewControlDiv.style.display = 'flex';
	viewControlDiv.style.borderRadius = '3px';
	viewControlDiv.style.cursor = 'pointer';
	viewControlDiv.style.marginTop = '10px';
	viewControlDiv.style.marginRight = '0px';
	var viewControl = new CustomMapview(viewControlDiv, map);

	// Insert the zoom div on the right left of the map
	map.controls[google.maps.ControlPosition.RIGHT_TOP].push(viewControlDiv);

	// Add custom buttons for the tools on the map
	function CustomZoomControl(controlDiv, map) {
	    // Graph the tool elements from the DOM and insert them in the map
	    var controlUiZoomIn = document.getElementById('zoom-in'),
	        controlUiZoomOut = document.getElementById('zoom-out'),
	        controlUiTraffic = document.getElementById('traffic'),
	        controlUiCircle = document.getElementById('circle');

	    controlDiv.appendChild(controlUiZoomIn);
	    controlDiv.appendChild(controlUiZoomOut);
	    controlDiv.appendChild(controlUiCircle);
	    controlDiv.appendChild(controlUiTraffic);

	    // Setup the click event listeners and zoom-in or out according to the clicked element
	    google.maps.event.addDomListener(controlUiZoomIn, 'click', function() {
	        map.setZoom(map.getZoom() + 1)
	    });
	    google.maps.event.addDomListener(controlUiZoomOut, 'click', function() {
	        map.setZoom(map.getZoom() - 1)
	    });
	    google.maps.event.addDomListener(controlUiTraffic, 'click', function() {
	        var trafficLayer = new google.maps.TrafficLayer();
	        trafficLayer.setMap(map);

	        //Trigger Traffic layer tool
	        $.get('ajax-filter-traffic', function(data) {
	            $.each(data, function(index, inventoryObject) {
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(inventoryObject.latitude, inventoryObject.longitude),
	                    map: map,
	                    category: inventoryObject.industry,
	                    type: inventoryObject.format,
	                    availability: inventoryObject.availability,
	                    company: inventoryObject.company,
	                    size: inventoryObject.size,
	                    product: inventoryObject.products,
	                    supplier: inventoryObject.supplier,
	                    visible: true,
	                    //animation: google.maps.Animation.DROP,
	                    icon: markerUrl
	                });

	                $('#markers').append('<li class="marker-link" data-markerid="' + inventoryObject.id +
						'" ><div class="markers-content"><a class="marker-link" data-markerid="' + index + '" href="#">' +
						inventoryObject.name + '</a><br/><small>' + inventoryObject.industry +
						'</small></div><img class="marker-link"  class="marker-link" src="uploads/inventory-images/' +
						inventoryObject.inventory_image + '"/></li>');

	                // Click Text format trigger marker function
	                $('.marker-link').on('click', function() {
	                    google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
	                });

	                google.maps.event.addListener(marker, 'click', (function(marker, i) {
	                    return function() {
	                        triggerInventory(true);
	                        $("#name").text(inventoryObject.name);
	                        $("#format").text(inventoryObject.format);
	                        $("#size").text(inventoryObject.size);
	                        $("#availability").text(inventoryObject.availability);
	                        $("#product").text(inventoryObject.products);
	                        $("#industry").text(inventoryObject.industry);
	                        $("#rates").text(inventoryObject.rates);
							$(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObject.inventory_image);

							// Modal content
	                        $("#inventory-name").val(inventoryObject.name);
	                        $("#inventory-name-text").text(inventoryObject.name);
	                        $("#inventory-supplier").val(inventoryObject.supplier);
	                        $("#inventory-supplier-text").text(inventoryObject.supplier);
	                        $("#inventory-user").val(inventoryObject.user.first_name);
	                        $("#inventory-user-text").text(inventoryObject.user.first_name);
	                        $("#inventory-company").val(inventoryObject.company);
	                        $("#inventory-company-text").text(inventoryObject.company);
	                        $("#inventory-rate").val(inventoryObject.rates);
	                        $("#inventory-rate-text").text(inventoryObject.rates);

							// Modal content hidden field value
	                        $("#receiver_email").val(inventoryObject.user.email);
	                        $("#receiver_id").val(inventoryObject.user.id);
	                        $("#inventory_img").val(inventoryObject.inventory_image);
	                        $("#bookmark").attr('href', "bookmark/" + inventoryObject.id);

							map.setZoom(18);
	                        map.panTo(marker.position);
	                    }
	                })(marker, inventoryObject.id));

	                markers.push(marker);

	                //console.log(inventoryObject);

	            });
	        });
	    });

	    google.maps.event.addDomListener(controlUiCircle, 'click', function() {
	        var circle = new google.maps.Circle({
	            map: map,
	            center: map.getCenter(),
	            radius: 6000,
	            fillColor: '#4590C5',
	            strokeOpacity: 0.8,
	            strokeWeight: 0,
	            editable: true,
	            draggable: true,
	        });

	        // Show marker if inside circle radius
	        google.maps.event.addListener(circle, 'drag', function(e) {
	            markers.forEach(function(marker) {
	                if (circle.getBounds().contains(marker.getPosition()) == true) {
	                    marker.setVisible(true);
	                } else {
	                    marker.setVisible(false);
	                }
	            });
	        });

	        $.get('ajax-filter-circle', function(data) {
	            $.each(data, function(index, inventoryObject) {
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(inventoryObject.latitude, inventoryObject.longitude),
	                    map: map,
	                    category: inventoryObject.industry,
	                    type: inventoryObject.format,
	                    availability: inventoryObject.availability,
	                    company: inventoryObject.company,
	                    size: inventoryObject.size,
	                    product: inventoryObject.products,
	                    supplier: inventoryObject.supplier,
	                    visible: false,
	                    //animation: google.maps.Animation.DROP,
	                    icon: markerUrl
	                });

	                $('#markers').append('<li class="marker-link" data-markerid="' + inventoryObject.id +
						'" ><div class="markers-content"><a class="marker-link" data-markerid="' + index + '" href="#">' +
						inventoryObject.name + '</a><br/><small>' + inventoryObject.industry +
						'</small></div><img class="marker-link"  class="marker-link" src="uploads/inventory-images/' +
						inventoryObject.inventory_image + '"/></li>');

	                // Click Text format trigger marker function
	                $('.marker-link').on('click', function() {
	                    google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
	                });

	                google.maps.event.addListener(marker, 'click', (function(marker, i) {
	                    return function() {
	                        triggerInventory(true);
	                        $("#name").text(inventoryObject.name);
	                        $("#format").text(inventoryObject.format);
	                        $("#size").text(inventoryObject.size);
	                        $("#availability").text(inventoryObject.availability);
	                        $("#product").text(inventoryObject.products);
	                        $("#industry").text(inventoryObject.industry);
	                        $("#rates").text(inventoryObject.rates);
	                        $(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObject.inventory_image);

							// Modal content
	                        $("#inventory-name").val(inventoryObject.name);
	                        $("#inventory-name-text").text(inventoryObject.name);
	                        $("#inventory-supplier").val(inventoryObject.supplier);
	                        $("#inventory-supplier-text").text(inventoryObject.supplier);
	                        $("#inventory-user").val(inventoryObject.user.first_name);
	                        $("#inventory-user-text").text(inventoryObject.user.first_name);
	                        $("#inventory-company").val(inventoryObject.company);
	                        $("#inventory-company-text").text(inventoryObject.company);
	                        $("#inventory-rate").val(inventoryObject.rates);
	                        $("#inventory-rate-text").text(inventoryObject.rates);

							// Modal content hidden field value
	                        $("#receiver_email").val(inventoryObject.user.email);
	                        $("#receiver_id").val(inventoryObject.user.id);
	                        $("#inventory_img").val(inventoryObject.inventory_image);
	                        $("#bookmark").attr('href', "bookmark/" + inventoryObject.id);

							map.setZoom(18);
	                        map.panTo(marker.position);
	                    }
	                })(marker, inventoryObject.id));

	                markers.push(marker);

	                //console.log(inventoryObject);
	            });
	        });
	    });
	}

	// Page stop reload if hit enter key
	$('#search').on('keyup keypress', function(e) {
	    var keyCode = (e.keyCode || e.which);
	    if (keyCode === 13) {
	        e.preventDefault();
	        return false;
	    }
	});

	var zoomControlDiv = document.createElement('div');
	var zoomControl = new CustomZoomControl(zoomControlDiv, map);

	// Insert the zoom div on the right left of the map
	map.controls[google.maps.ControlPosition.RIGHT_TOP].push(zoomControlDiv);

	// Create the search box and link it to the UI element.
	var input = document.getElementById('search');
	var searchBox = new google.maps.places.SearchBox(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
	    searchBox.setBounds(map.getBounds());
	});

	var markers = [];
	// Listen for the event fired when the user selects a prediction
	// and retrieve more details for that place.
	searchBox.addListener('places_changed', function() {
	    var places = searchBox.getPlaces();

	    if (places.length == 0) {
	        return;
	    }

	    // Clear out the old markers.
	    markers.forEach(function(marker) {
	        marker.setMap(map);
	    });

	    //markers = [];

	    // For each place, get the icon, name and location.
	    var bounds = new google.maps.LatLngBounds();
	    places.forEach(function(place) {
	        var icon = {
	            url: markerUrl,
	            //size: new google.maps.Size(71, 71),
	            //origin: new google.maps.Point(0, 0),
	            //anchor: new google.maps.Point(17, 34),
	            //scaledSize: new google.maps.Size(25, 25)
	        };

			/*
	        // Create a marker for each place.
	        markers.push(new google.maps.Marker({
	        	map: map,
	        	icon: icon,
	        	title: place.name,
	        	visible: false,
	        	position: place.geometry.location
	        }));
			*/

	        if (place.geometry.viewport) {
	            // Only geocodes have viewport.
	            bounds.union(place.geometry.viewport);
	        } else {
	            bounds.extend(place.geometry.location);
	        }
	    });
	    map.fitBounds(bounds);
	});

	var locations;

	// Trigger Filter Basic & Demographic
	$('.filter').on('change', function(e) {
	    e.stopPropagation();
	    e.preventDefault();

	    console.log(e);

	    var filter_id = e.target.value;
	    map.setZoom(11);

	    console.log(filter_id);

	    if (this.checked) {
	        //alert("before push: " + markers.length);
	        $.get('ajax-filter-inventory?filter_id=' + filter_id, function(data) {
	            $.each(data, function(index, inventoryObject) {
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(inventoryObject.latitude, inventoryObject.longitude),
	                    map: map,
	                    name: inventoryObject.name,
	                    category: inventoryObject.industry,
	                    type: inventoryObject.format,
	                    availability: inventoryObject.availability,
	                    company: inventoryObject.company,
	                    size: inventoryObject.size,
	                    product: inventoryObject.products,
	                    supplier: inventoryObject.supplier,
	                    gender: inventoryObject.gender,
	                    economic_class: inventoryObject.economic_class,
	                    age: inventoryObject.age,
	                    profile: inventoryObject.profile,
	                    city: inventoryObject.city,
	                    visible: true,
	                    id: inventoryObject.id,
	                    //animation: google.maps.Animation.DROP,
	                    icon: markerUrl
	                });

	                google.maps.event.addListener(marker, 'click', (function(marker, data) {
	                    return function() {
	                        triggerInventory(true);
	                        $("#name").text(inventoryObject.name);
	                        $("#format").text(inventoryObject.format);
	                        $("#size").text(inventoryObject.size);
	                        $("#availability").text(inventoryObject.availability);
	                        $("#product").text(inventoryObject.products);
	                        $("#industry").text(inventoryObject.industry);
	                        $("#rates").text(inventoryObject.rates);
	                        $(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObject.inventory_image);

							// Modal content
	                        $("#inventory-name").val(inventoryObject.name);
	                        $("#inventory-name-text").text(inventoryObject.name);
	                        $("#inventory-supplier").val(inventoryObject.supplier);
	                        $("#inventory-supplier-text").text(inventoryObject.supplier);
	                        $("#inventory-user").val(inventoryObject.user.first_name);
	                        $("#inventory-user-text").text(inventoryObject.user.first_name);
	                        $("#inventory-company").val(inventoryObject.company);
	                        $("#inventory-company-text").text(inventoryObject.company);
	                        $("#inventory-rate").val(inventoryObject.rates);
	                        $("#inventory-rate-text").text(inventoryObject.rates);

							// Modal content hidden field value
	                        $("#receiver_email").val(inventoryObject.user.email);
	                        $("#receiver_id").val(inventoryObject.user.id);
	                        $("#inventory_img").val(inventoryObject.inventory_image);
	                        $("#bookmark").attr('href', "bookmark/" + inventoryObject.id);

							map.setZoom(18);
	                        map.panTo(marker.position);
	                        //Change marker icon onclick
	                        marker.setIcon(markerDarkUrl);
	                    }
	                })(marker, index));

	                markers.push(marker);

	                $('#markers').append('<li class="marker-link" data-markerid="' + index + '" data-marker="' + filter_id +
					 	'"><div class="markers-content"><a class="marker-link" data-markerid="' + index + '" data-marker="' +
						 filter_id + '" href="#">' + inventoryObject.name + '</a><br/><small>' + inventoryObject.industry +
						  '</small></div><img src="uploads/inventory-images/' + inventoryObject.inventory_image + '"/></li>');

	                // Click Text format trigger marker function
	                $('.marker-link').on('click', function() {
	                    google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
	                });

	                //console.log(inventoryObject);

	            });
	            //alert("after push: " + markers.length);
	        });

	    } else {
	        //alert("before trim: " + markers.length);
	        var unselected = [];

	        markers.forEach(function(marker, index) {
	            if (marker.category == filter_id) {
	                marker.setMap(null);
	                map.setZoom(11);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.type == filter_id) {
	                marker.setMap(null);
	                map.setZoom(11);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.availability == filter_id) {
	                marker.setMap(null);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.gender == filter_id) {
	                marker.setMap(null);
	                map.setZoom(11);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.economic_class == filter_id) {
	                marker.setMap(null);
	                map.setZoom(11);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.age == filter_id) {
	                marker.setMap(null);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.profile == filter_id) {
	                marker.setMap(null);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }
	            if (marker.city == filter_id) {
	                marker.setMap(null);
	                map.setZoom(11);
	                $('.marker-link[data-marker="' + filter_id + '"]').remove();
	            }

	            // CARE REFACTORING AND CONVERTING TO IF-ELSE IF,
	            // ISSUE MAY ARISE WHEN MULTIPLE FILTERS ARE ACTIVE.
	            if (marker.category == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.type == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.availability == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.gender == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.economic_class == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.age == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.profile == filter_id) {
	                unselected.push(index);
	            }
	            if (marker.city == filter_id) {
	                unselected.push(index);
	            }
	        });

	        // Remove duplicates in O(n): linear time using Hashtable!
	        var seen = [];
	        unselected = unselected.filter(function(item) {
	            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
	        });

	        // Remove markers. Take into consideration
	        // array adjustment during splice.
	        for (var i = 0; i < unselected.length; i++) {
	            markers.splice((unselected[i] - i), 1);
	        }

	        //alert("after trim: " + markers.length);
	    }
	});


	//$('#search').change(function() {
	//alert( "Handler for .change() called." );
	//});

	// Search Trigger Function
	$('#search').keypress(function(e) {
	    console.log(e);

	    var search_id = e.target.value;
	    var clear_id = e.target.value;

	    if (e.which == 13) {

	        $.get('ajax-search?search_id=' + search_id, function(data) {
	            console.log(data);
	            $.each(data, function(index, inventoryObject) {
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(inventoryObject.latitude, inventoryObject.longitude),
	                    map: map,
	                    category: inventoryObject.industry,
	                    type: inventoryObject.format,
	                    availability: inventoryObject.availability,
	                    company: inventoryObject.company,
	                    size: inventoryObject.size,
	                    product: inventoryObject.products,
	                    supplier: inventoryObject.supplier,
	                    name: inventoryObject.name,
	                    visible: true,
	                    //animation: google.maps.Animation.DROP,
	                    icon: markerUrl
	                });

	                google.maps.event.addListener(marker, 'click', (function(marker, i) {
	                    return function() {
	                        triggerInventory(true);
	                        $("#name").text(inventoryObject.name);
	                        $("#format").text(inventoryObject.format);
	                        $("#size").text(inventoryObject.size);
	                        $("#availability").text(inventoryObject.availability);
	                        $("#industry").text(inventoryObject.industry);
	                        $("#rates").text(inventoryObject.rates);
	                        $(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObject.inventory_image);

							// modal content
	                        $("#inventory-name").val(inventoryObject.name);
	                        $("#inventory-name-text").text(inventoryObject.name);
	                        $("#inventory-supplier").val(inventoryObject.supplier);
	                        $("#inventory-supplier-text").text(inventoryObject.supplier);
	                        $("#inventory-user").val(inventoryObject.user.first_name);
	                        $("#inventory-user-text").text(inventoryObject.user.first_name);
	                        $("#inventory-company").val(inventoryObject.company);
	                        $("#inventory-company-text").text(inventoryObject.company);
	                        $("#inventory-rate").val(inventoryObject.rates);
	                        $("#inventory-rate-text").text(inventoryObject.rates);

							// Modal content hidden field value
	                        $("#receiver_email").val(inventoryObject.user.email);
	                        $("#receiver_id").val(inventoryObject.user.id);
	                        $("#inventory_img").val(inventoryObject.inventory_image);
	                        $("#bookmark").attr('href', "bookmark/" + inventoryObject.id);

							map.setZoom(18);
	                        map.panTo(marker.position);
	                    }
	                })(marker, inventoryObject.id));

	                markers.push(marker);
	            });
	        });
	    };
	});


	// Trigger data on click auto complete
	$(function() {
	    $("#search").autocomplete({
	        source: "ajax-autocomplete",
	        minLength: 3,
	        select: function(event, data) {
	            console.log(data.item.value);

	            var data_id = data.item.value;

	            //$('#markers li').remove();

	            $.get('ajax-autocomplete-select?data_id=' + data_id, function(data) {
	                console.log(data);
	                $.each(data, function(index, inventoryObject) {
	                    marker = new google.maps.Marker({
	                        position: new google.maps.LatLng(inventoryObject.latitude, inventoryObject.longitude),
	                        map: map,
	                        category: inventoryObject.industry,
	                        type: inventoryObject.format,
	                        availability: inventoryObject.availability,
	                        company: inventoryObject.company,
	                        size: inventoryObject.size,
	                        product: inventoryObject.products,
	                        supplier: inventoryObject.supplier,
	                        name: inventoryObject.name,
	                        visible: true,
	                        //animation: google.maps.Animation.DROP,
	                        icon: markerUrl
	                    });

	                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
	                        return function() {
	                            triggerInventory(true);
	                            $("#name").text(inventoryObject.name);
	                            $("#format").text(inventoryObject.format);
	                            $("#size").text(inventoryObject.size);
	                            $("#availability").text(inventoryObject.availability);
	                            $("#product").text(inventoryObject.products);
	                            $("#industry").text(inventoryObject.industry);
	                            $("#rates").text(inventoryObject.rates);
	                            $(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObject.inventory_image);

								// Modal content
	                            $("#inventory-name").val(inventoryObject.name);
	                            $("#inventory-name-text").text(inventoryObject.name);
	                            $("#inventory-supplier").val(inventoryObject.supplier);
	                            $("#inventory-supplier-text").text(inventoryObject.supplier);
	                            $("#inventory-user").val(inventoryObject.user.first_name);
	                            $("#inventory-user-text").text(inventoryObject.user.first_name);
	                            $("#inventory-company").val(inventoryObject.company);
	                            $("#inventory-company-text").text(inventoryObject.company);
	                            $("#inventory-rate").val(inventoryObject.rates);
	                            $("#inventory-rate-text").text(inventoryObject.rates);

								// Modal content hidden field value
	                            $("#receiver_email").val(inventoryObject.user.email);
	                            $("#receiver_id").val(inventoryObject.user.id);
	                            $("#inventory_img").val(inventoryObject.inventory_image);
	                            $("#bookmark").attr('href', "bookmark/" + inventoryObject.id);

								map.setZoom(18);
	                            map.panTo(marker.position);
	                        }
	                    })(marker, inventoryObject.id));

	                    markers.push(marker);

	                    $('#markers').append('<li class="marker-link"data-markerid="' + index + '" data-marker="' + data_id +
							'"><div class="markers-content"><a class="marker-link" data-markerid="' + index + '" href="#">' +
							inventoryObject.name + '</a><br/><small>' + inventoryObject.industry +
							'</small></div><img src="uploads/inventory-images/' + inventoryObject.inventory_image + '"/></li>');

	                    // Click Text format trigger marker function
	                    $('.marker-link').on('click', function() {
	                        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
	                    });
	                });
	            });
	        }
	    });
	});

	// Click Zoom out
	$('#close').on('click', function() {
	    map.setZoom(11);
	});

	function triggerInventory($bool) {
	    var elementsToTrigger = $([$('.inventory')]);
	    elementsToTrigger.each(function() {
	        $(this).toggleClass('inventory-is-visible', $bool);
	    });
	}
</script>
