<footer id="contact" class="col-md-12">
	<div class="row">
		<div class="col-md-4">
			<img src="{{ URL::to('img/logo-white.png') }}" alt="" style="margin-bottom: 1em;">
			<p>is a platform that bonds together local OOH market data and proprietary insights to bring about efficient and effective planning for Out Of Home campaigns</p>
		</div>
		<div class="col-md-4">
			<h3 style="margin-left:43px;"><small>Company</small></h3>
			<ul class="list-inline">
				<li>
					<a class="page-scroll" href="{{ URL::to('/')}}">Home</a>
				</li>
				<li>
					<a class="page-scroll" href="#about">About</a>
				</li>
				<li>
					<a class="page-scroll" href="#services">Services</a>
				</li>
				<li>
					<a class="page-scroll" href="#news">News</a>
				</li>
				<li>
					<a class="page-scroll" href="#contact">Contact</a>
				</li>
			</ul>
			<div class="row footer-info-container">
				<div class="col-md-4 footer-info footer-info-mail">
					<div class="footer-icon"><i class="fa fa-envelope text-white"></i></div>
					<a href="mailto:info@itoohl.com" class="text-white">info@itoohl.com</a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<h3><small>Request for Demo</small></h3>
			<form action="{{ URL::route('subscribe') }}" method="post" class="form-group">
				<input type="email" name="email" class="form-control" required>
				<button type="submit" class="subscribe btn btn-primary form-control">Subscribe</button>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</footer>	