<header class="main-header">
    <!-- Logo -->
    <a href="{{ URL::route('user-dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><img src="{{ URL::to('img/itoohl-logo-mini.png')}}" style="width: 30px;"></span> -->
        <span class="logo-mini"><img src="{{ URL::to('img/newLogo.png')}}" style="height: 50px;"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{ URL::to('img/newLogo.png')}}" style="height: 50px;"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle chat-menu" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success"></span>
                    </a>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu" id="notify_read_all" >
                    <a href="javascript:void(0)"  class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        @if( !empty($notification_count))
                            <span class="label label-warning" id="notify-count">{{ $notification_count }}</span>
                        @endif
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have {{ $notification_count }} notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @foreach($notifications as $notification)
                                    <li>
                                    @include('notifications.' . snake_case(class_basename($notification->type)))
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer"><a href="{{ URL::route('profile') }}">View all</a></li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ URL::to('uploads/avatars/' . Auth::user()->avatar) }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->first_name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                    <!-- User image -->
                        <li class="user-header">
                            <img src="{{ URL::to('uploads/avatars/' . Auth::user()->avatar) }}" class="img-circle" alt="User Image">
                            <p>
                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} - 
                                @if( Auth::user()->role == 1 ) 
                                    Admin
                                @elseif( Auth::user()->role == 2 )
                                    Advertiser
                                @else
                                    Vendor
                                @endif
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ URL::route('profile')}}" class="btn btn-default btn-flat btn-yellow">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ URL::route('admin-logout') }}" class="btn btn-default btn-flat btn-yellow">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>