<div class="modal fade modal-primary in" id="request-quotes" tabindex="-1" role="dialog" aria-labelledby="request-quotesLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="text-white">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ URL::route('request-quotes') }}" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="img-responsive inventory-image" src="" alt="">
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <b>Name:</b>
                                            <span id="inventory-name-text"></span>
                                        </div>
                                        <div class="form-group">
                                            <b>Published Rates:</b>
                                            <span id="inventory-rate-text"></span>
                                        </div>  
                                        <div class="form-group">
                                            <b>Owner:</b>
                                            <span id="inventory-user-text"></span>
                                        </div>
                                        <div class="form-group">
                                            <b>Company:</b>
                                            <span id="inventory-company-text"></span>
                                        </div>
                                        <div class="form-group">
                                            <b>Supplier:</b>
                                            <span id="inventory-supplier-text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Hidden Fields -->
                            <input type="hidden" id="inventory-name" name="inventory_name" value="">
                            <input type="hidden" id="inventory-rate" name="inventory_rate">
                            <input type="hidden" id="inventory-user" name="receiver_name">
                            <input type="hidden" id="inventory-company" name="inventory_company">
                            <input type="hidden" id="inventory-supplier" name="inventory_supplier">

                            <input type="hidden" id="receiver_email" name="receiver_email" value="">
                            <input type="hidden" id="receiver_id" name="receiver_id" value="">
                            <input type="hidden" id="inventory_img" name="inventory_img" value="">

                            <hr>

                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Date range -->
                                    <div class="form-group">
                                        <label>Placement Duration</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="placement_duration" class="form-control pull-right" id="reservation">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="material_changes">Material Changes</label>
                                        <select class="form-control select2" name="material_changes"  data-placeholder="Material Changes" style="width: 100%;">
                                        @foreach($materialchanges as $materialchange)
                                            <option value="{{$materialchange->name}}">{{$materialchange->name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Date -->
                                    <div class="form-group">
                                        <label>Date of Material Changes</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="material_changes_date" class="form-control pull-right" id="material_changes">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->              
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="production">Productions</label>
                                            <select class="form-control select2" name="production"  data-placeholder="Productions" style="width: 100%;">
                                                @foreach($productions as $production)
                                                    <option value="{{$production->name}}">{{$production->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="instruction">Special Instruction</label>
                                        <textarea class="form-control" name="instruction" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn quotes-btn pull-right" >Request Quote</button>
                                    {{ csrf_field() }}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
