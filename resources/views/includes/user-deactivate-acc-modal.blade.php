<div class="modal fade" id="deactivate-acc" tabindex="-1" role="dialog" aria-labelledby="request-quotesLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="request-quotesLabel">Deactivate Account</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="{{ URL::route('deactivate-accnt') }}"  method="post">
                    <div class="box-body">
                        <div class="col-xs-12">
                            <h4 class="text-center">
                                Quisque velit nisi, pretium ut lacinia in, elementum id enim. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.
                            </h4>
                        </div>
                        <div class="form-group">
                            <label for="material_changes">Reason:</label>
                            <textarea class="form-control"  name="reason" rows="4" placeholder="Enter ..."></textarea>
                        </div> 
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        {{ csrf_field() }}
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>


