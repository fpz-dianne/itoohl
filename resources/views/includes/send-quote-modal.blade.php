<div class="modal modal-primary fade in" id="send-quote" tabindex="-1" role="dialog" aria-labelledby="request-quotesLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="text-white">&times;</span></button>
                    <h4 class="modal-title" id="request-quotesLabel">Send Quote</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="{{ URL::to('send-quote/' . $request_quote->id) }}"  method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="quotation">Quotation</label>
                                <input type="text" class="form-control" name="quotation" id="quotation" placeholder="">
                            </div> 
                            <div class="form-group">
                                <label for="material_changes">Note</label>
                                <textarea class="form-control"  name="note" rows="3" placeholder="Enter ..."></textarea>
                            </div> 
                       
                            <!-- hidden fields  --> 
                            <input type="hidden" name="placement_duration" class="form-control pull-right reservation" value="{{ $request_quote->placement_duration  }}" id="reservation2">
                            <input type="hidden" class="form-control" name="material_changes" id="material_changes" value="{{ $request_quote->material_changes }}"> 
                            <input type="hidden" name="material_changes_date" class="form-control pull-right material_changes_date" id="material_changes_date" value="{{ $request_quote->material_changes_date }}">
                            <input type="hidden" class="form-control" name="production"  id="production" value="{{ $request_quote->production }}">
                            <input type="hidden" class="form-control"  name="instruction" value="{{ $request_quote->instruction }}"></input>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            {{ csrf_field() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


