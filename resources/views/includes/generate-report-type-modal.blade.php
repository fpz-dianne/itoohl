<div class="modal fade modal-primary in" id="generate-report-type">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                    <h4 class="modal-title">Generate Report</h4>
                </div>
                <div class="modal-body">
                    <a href="{{ URL::route('generate-report') }}">
                        <div class="info-box box-info">
                            <span class="info-box-icon bg-white" style="height: 86px;"><i class="fa fa-bar-chart-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Single Reports</span>
                            </div>
                        </div>
                    </a>

                    <a href="{{ URL::route('generate-report-compare') }}">
                        <div class="info-box box-info">
                            <span class="info-box-icon bg-white" style="height: 86px;"><i class="fa fa-bar-chart-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Compare Reports</span>
                            </div>
                        </div>
                    </a>
                <!-- <a href="{{ URL::route('generate-report-sales') }}">
                <div class="info-box box-info">
                <span class="info-box-icon bg-white" style="height: 86px;"><i class="fa fa-bar-chart-o"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Sales Report</span>
                </div>
                </div>
                </a> -->
                </div>
            </div>
        </div>
    </div>
</div>