<div class="col-md-3">
	<div class="row">
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>

				<h3>Category</h3>
				<!-- /.search form -->
				<ul class="sidebar-menu bg-white">
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i>
							<span>Apparel</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i>
							<span>Beverages</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i> <span>Technology</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i> <span>Tables</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
							<li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i> <span>Insights</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i> <span>News</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-angle-right"></i> <span>Events</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
							<li><a href="javascript::void(0)"><i class="fa fa-circle-o"></i> Sample Sub</a></li>
						</ul>
					</li>
				</ul>
			</section>
		</aside>
	</div>

	<div class="row single-news-popular">
		<h3>Popular Post</h3>
		<img class="img-responsive" src="{{ URL::to('img/news-item-1.jpg') }}" alt="">
		<h4>Title here</h4>
		<p>By: Robert Cruda</p>

		<img class="img-responsive" src="{{ URL::to('img/news-item-1.jpg') }}" alt="">
		<h4>Title here</h4>
		<p>By: Robert Cruda</p>
	</div>

	<div class="row single-news-tags">
		<h3>Popular Tags</h3>
		<button type="button" class="btn">Advertising</button>
		<button type="button" class="btn">Billboards</button>
		<button type="button" class="btn" style="margin-top: 3px;">News</button>
	</div>
</div>