@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Profile
@endsection

@section('dashboard-title')
  User Profile
@endsection

@section('content')

@include('includes.user-deactivate-acc-modal')
	   <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary"> 
            <div class="box-body box-profile">
              
              <img class="profile-user-img img-responsive img-circle" src="{{ URL::to('uploads/avatars/' . $user->avatar) }}" alt="User profile picture">

              <h3 class="profile-username text-center">{{ $user->first_name }} {{ $user->last_name }}</h3>

              <p class="text-muted text-center">
              	@if( $user->role == 1 ) 
                      Admin
                    @elseif( $user->role == 2 )
                      Advertiser
                    @else
                      Vendor
                  @endif
              </p>

              <hr>

              <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

              <p class="text-muted">
                {{ $user->email }}
              </p>

              <hr>

              <strong><i class="fa fa-building margin-r-5"></i> Company</strong>

              <p class="text-muted">
                {{ $user->company }}
              </p>

              <hr>

              <strong><i class="fa fa-phone margin-r-5"></i> Phone</strong>

              <p class="text-muted">
                {{ $user->phone }}
              </p>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#inventory" data-toggle="tab">Inventory</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="inventory">
               @if( !$portals->isEmpty() )
                  @foreach(array_chunk($portals->all(), 2) as $row)
                  <div class="row">
                    @foreach($row as $portal)
                      <div class="col-md-6">
                        <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user-2">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="widget-inventory-header"  style="background: url('../uploads/inventory-images/{{ $portal->inventory_image }}' )">
                                <!-- /.widget-user-image -->
                                <h3 class="widget-inventory-name">{{ $portal->name }}</h3>
                              </div>
                              <div class="box-footer no-padding">
                                <ul class="nav nav-stacked">
                                  <li><a href="#">Supplier <span class="pull-right badge bg-blue">{{ $portal->supplier }}</span></a></li>
                                  <li><a href="#">Format <span class="pull-right badge bg-blue">{{ $portal->format }}</span></a></li>
                                  <li><a href="#">Location <span class="pull-right badge bg-blue"> {{ $portal->street_address }}, {{ $portal->city }}</span></a></li>
                                  <li><a href="#">Industry <span class="pull-right badge bg-blue">{{ $portal->industry }}</span></a></li>
                                </ul>
                            </div>
                          </div>
                      </div>
                    @endforeach
                </div>
                @endforeach

                <div class="row">
                  <div class="inventory-pager">
                    {{ $portals->links() }}
                  </div>
                </div>
                
                @else
                <div class="row">
                  <div class="div-col-md-12">
                    <h1 class="text-center">No Records Found</h1>
                  </div>
                </div>
               @endif
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>

@endsection

@section('script')
<script>
$('input[type=file]').change(function() {
	var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
	$('#file-name').html(filename);
});
</script>

<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#file-image').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$('input[type=file]').change(function(){
	if ($(this).get(0).files.length != 0) {
		readURL(this);
		$('#file-image').css('display', '');
		$('#upload-photo').css('display', '');
		$('#delete-photo').css('display', 'none');
	} else {
		$('#file-image').css('display', 'none');
		$('#upload-photo').css('display', 'none');
		$('#delete-photo').css('display', '');
	}
});
</script>

<script>
$('.delete-inventory').on('click', function(e){

	e.preventDefault();

	var deleteInventory = $(this).attr('href');

	swal({   
		title: "Are you sure?",
		text: "You will not be able to recover this lorem ipsum!",         
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Delete", 
		closeOnConfirm: false 
	}, 

	function(){   
		window.location.href = deleteInventory;
	});
})
</script>
@endsection