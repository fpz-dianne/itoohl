@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Demo
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
  Demo
@endsection

@section('content')
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if( !$demos->isEmpty() )
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="demo-list" class="table table-bordered table-hover">
                <thead>
                <tr class="bg-blue">
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Company</th>
                  <th>Purpose</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach( $demos as $demo )
                      <tr>
                        <td>{{ $demo->id }}</td>
                        <td>{{ $demo->name }}</td>
                        <td>{{ $demo->email }}</td>
                        <td>{{ $demo->company }}</td>
                        <td>
                          @if($demo->demooption)
                          {{$demo->demooption->name}}{{(($demo->dmeooption2||$demo->demooption->name!= "All of the above")?" and ".$demo->demooption2->name:"")}}
                          @endif
                        </td>
                        <td class="text-center">
                          <a href="{{  URL::to('demo-delete/'. $demo->id ) }}" class="delete-demo"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          @else
          <div class="box">
            <div class="box-body">
              <h1 class="text-center">No Records Found</h1>
            </div>
          </div>  
          @endif
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
@endsection

@section('script')

<script>
  //Delete demo
  $('.delete-demo').on('click', function(e){

  e.preventDefault();

  var deleteDemo = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteDemo;
  });
}); 
 
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#demo-list").DataTable({
     "sort": false
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

