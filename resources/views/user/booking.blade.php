@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Booking
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
  Booking
@endsection

@section('content')
	   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if( !$booking_memo->isEmpty() )
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="booking-list" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Inventory Name</th>
                  <th>Booking Memo for</th>
                  <th>Placement Duration</th>
                  <th>Material Changes</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach( $booking_memo as $booking )
                    <tr>
                      <td>{{$booking->name}}</td>
                      <td>{{$booking->receiver_email}}</td>
                      <td>{{$booking->placement_duration}}</td>
                      <td>{{$booking->material_changes}}</td>
                      <td><span class="label label-warning">Pending</span></td>
                      <td class="text-center">
                        <a href="{{ URL::to('booking/' . $booking->id ) }}" class=""><i class="fa fa-eye"></i></a>
                        <a href="{{ URL::to('booking-delete/' . $booking->id ) }}" class="delete-booking"><i class="fa fa-trash"></i></a>
                      </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        @else
        <div class="box">
          <div class="box-body">
            <h1 class="text-center">No Records Found</h1>
          </div>
        </div>
        @endif
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
@endsection

@section('script')

<script>
  $('.delete-booking').on('click', function(e){

  e.preventDefault();

  var deleteBooking = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteBooking;
  });
})
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#booking-list").DataTable({
      "ordering": false
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

