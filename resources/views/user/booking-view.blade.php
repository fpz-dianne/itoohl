@extends('layouts.user-dashboard-layout')

@section('style')
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{URL::to('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
@endsection

@section('title')
  iTOOhL | Booking Memo
@endsection

@section('dashboard-title')
   Booking Memo
@endsection

@section('content')

     <!-- Main content -->
  <section class="invoice" id="invoice">
    <div class="printonly">
      <h1>From Itoohl.com</h1>
      <div class="print-space"></div>
    </div>
    <!-- title row -->
    <div class="row">
      <div class="col-xs-6">
         <img class="img-responsive inventory-image" src="{{ URL::to('uploads/inventory-images/' . $booking->inventory_img )}}" alt="">
      </div>
      <div class="col-xs-6" style="margin-bottom: 1em;">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-map-marker"></i> {{ $booking->name }}
            <small class="pull-right">Date: {{ $booking->created_at->format('m/d/y') }}</small>
          </h2>
          <h4>From</h4>
          <address style="margin-bottom: 0;">
            <strong>{{ $booking->user->first_name }}</strong><br>
            Phone: {{ $booking->user->phone}}<br>
            Email: {{ $booking->user->email }}
          </address>
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
          <h4>Request</h4>
          <b>Placement Duration:</b> {{ $booking->placement_duration }}<br>
          <b>Material Changes:</b> {{ $booking->material_changes }}<br>
          <b>Date of Material Changes:</b> {{ $booking->material_changes_date }}<br>
          <b>Production:</b> 968-34567
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
          <h4>Special Instruction</h4>
          <div class="instruction-container col-sm-12">
            {{ $booking->instruction }}
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
          <h4>Memo</h4>
          <b>Confirmation:</b> {{ $booking->confirmation }}<br>
          <b>Final Rate:</b> {{ $booking->final_rate }}<br>
        </div>
        <!-- /.col -->
        </div>
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <button type="button" data-toggle="modal" data-target="#booking-memo" class="btn btn-primary pull-right">
           Approve
        </button>
        <a onclick="printContent('invoice')" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> Print</a>
      </div>
    </div>

  </section>
  <!-- /.content -->


@endsection

@section('script')

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::to('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Page script -->
<script>
  $(function () {
    // Date range picker
    $('#reservation').daterangepicker();
    // Date picker
    $('#material_changes_date').datepicker({
      autoclose: true
    });
  });
</script>

<script>
  $('.delete-inventory').on('click', function(e){
  e.preventDefault();
  var deleteInventory = $(this).attr('href');
  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteInventory;
  });
})

</script>

<script>

//print qoutes
function printContent(el){
  var restorepage = document.body.innerHTML;
  var printcontent = document.getElementById(el).innerHTML;
  document.body.innerHTML = printcontent;
  window.print();
  document.body.innerHTML = restorepage;
}
</script>

@endsection

