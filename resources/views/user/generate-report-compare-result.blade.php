@extends('layouts.user-dashboard-layout')


@section('title')
iTOOhL | Result
@endsection

@section('style')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ URL::to('plugins/morris/morris.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ URL::to('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection

@section('dashboard-title')
Result
@endsection

@section('content')

@include('includes.generate-report-type-modal')
<!-- Main row -->
<div class="row">
  <!-- Left col -->
  <section class="col-lg-6 connectedSortable">

    <div class="box box-default">
      <div class="box box-default" id="Inventory1">
        <div class="printonly">
          <h1>From Itoohl.com</h1>
          <div class="print-space"></div>
        </div>
        <div class="box-header with-border bg-light-blue">
          @if( $results_1_user != 'OOH Inventory' )
            <h3 class="box-title">{{$results_1_user->first_name}} {{$results_1_user->last_name}}</h3>
            @else
            <h3 class="box-title">OOH Inventory</h3>
          @endif

          <div class="box-tools pull-right hideprint">
           <a onclick="printContent('Inventory1')"><button type="button" class="btn btn-box-tool"><i class="fa fa-print" style="color: #fff;"></i>
           </button></a>
         </div>
       </div>
       <div id="browser-box-content">
        <!-- /.box-header -->
        @if (!$results_1->isEmpty()) 
          <div class="box-body">
          <div class="row">
            @if( $chart == 'pieChart' )
              <div class="col-md-8">
                <div class="chart-responsive">
                  <canvas class="hideprint" id="{{ $chart }}1" height1="150"></canvas>
                  <img id='snapshotImageElement'/>
                </div>
                <!-- ./chart-responsive -->
              </div>
              <!-- /.col -->
              
              <div class="col-md-4">
                <div id="legend-1" class="chart-legend">
                </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            @else
              <div class="col-md-12">
                <div class="chart-responsive">
                  <canvas class="hideprint" id="{{ $chart }}1" height1="150"></canvas>
                  <img id='snapshotImageElement'/>
                </div>
                <!-- ./chart-responsive -->
              </div>
              <!-- /.col -->
            @endif
        </div>
          @else
          <div class="box-body">
            <h1 class="text-center">Data not Found</h1>
            <div class="row">
            <div class="col-md-8">
              <div class="chart-responsive">
                <canvas class="hideprint" id="{{ $chart }}1" height1="150"></canvas>
                <img id='snapshotImageElement'/>
              </div>
              <!-- ./chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <ul class="chart-legend clearfix">
                @foreach($results_1 as $result_1)
                  <li><i class="fa fa-circle text-red"></i> {{$result_1->$data_1}}</li>
                @endforeach
              </ul>
            </div>
            <!-- /.col -->
          </div>
          </div>
        @endif  
        <!-- /.box-body -->
        <div class="box-footer no-padding">
          <ul class="nav nav-pills nav-stacked">
            @foreach( $results_1 as $result_1 )
              <li><a href="#">{{ $result_1->$data_1 }}<span class="pull-right text-red">{{ $result_1->data_count }}</span></a></li> 
            @endforeach
              <li class="no-hover"><a href="#" class="text-aqua"><b>Total</b><span class="pull-right text-aqua"><b>{{ $data_1_total }}</b></span></a></li>
          </ul>
            </div>
            <!-- /.footer -->
          </div>
        </div>
        <!-- /.box -->
      </section>
  <!-- /.Left col -->
<div class="page-break"></div>
  <!-- right col (We are only adding the ID to make the widgets sortable)-->
<section class="col-lg-6 connectedSortable">

    <div class="box box-default">
      <div class="box box-default" id="Inventory2">
        <div class="printonly">
          <h1>From Itoohl.com</h1>
          <div class="print-space"></div>
        </div>
        <div class="box-header with-border bg-light-blue">
          @if( $results_2_user != 'OOH Inventory' )
            <h3 class="box-title">{{$results_2_user->first_name}} {{$results_2_user->last_name}}</h3>
            @else
            <h3 class="box-title">OOH Inventory</h3>
          @endif

          <div class="box-tools pull-right hideprint">
           <a onclick="printContent('Inventory2')"><button type="button" class="btn btn-box-tool"><i class="fa fa-print" style="color: #fff;"></i>
           </button></a>
         </div>
       </div>
       <div id="browser-box-content">
        <!-- /.box-header -->
        @if (!$results_2->isEmpty() )
          <div class="box-body">
            <div class="row">
              @if( $chart == 'pieChart' )
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas class="hideprint" id="{{ $chart }}2" height="150"></canvas>
                    <img id='snapshotImageElement1'/>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div id="legend-2" class="chart-legend"></div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              @else
                <div class="col-md-12">
                  <div class="chart-responsive">
                    <canvas class="hideprint" id="{{ $chart }}2" height="150"></canvas>
                    <img id='snapshotImageElement1'/>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
              @endif
          </div>
          @else
          <div class="box-body">
            <h1 class="text-center">Data not Found</h1>
            <div class="row">
              <div class="col-md-8">
                <div class="chart-responsive">
                  <canvas class="hideprint" id="{{ $chart }}2" height="150"></canvas>
                  <img id='snapshotImageElement1'/>
                </div>
                <!-- ./chart-responsive -->
              </div>
              <!-- /.col -->
              <div id="js-legend" class="chart-legend col-md-4">
              </div>
              <div class="col-md-4">
                <ul class="chart-legend clearfix">
                  @foreach($results_2 as $result_2)
                    <li><i class="fa fa-circle text-red"></i> {{$result_2->$data_2}}</li>
                  @endforeach
                </ul>
              </div>
              <!-- /.col -->
            </div>
          </div>
        @endif
        <!-- /.box-body -->
        <div class="box-footer">
          <ul class="nav nav-pills nav-stacked">
            @foreach( $results_2 as $result_2 )
               <li><a href="#">{{ $result_2->$data_2 }}<span class="pull-right text-red">{{ $result_2->data_count }}</span></a></li>  
            @endforeach
              <li class="no-hover"><a href="#" class="text-aqua"><b>Total</b><span class="pull-right text-aqua"><b>{{ $data_2_total }}</b></span></a></li>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
        </div>
        <!-- /.box -->
      </section>
      <!-- right col -->
      <section class="box-footer col-lg-12 hideprint">
          <a href="" class="btn btn-primary pull-right" onclick="printContent('print-all')" style="margin-left: 5px;">Print</a>
          <a href="{{ URL::route('generate-report-compare') }}" class="btn btn-primary pull-right">Generate More Report</a>
      </section>
    </div>
    <!-- /.row (main row) -->

    @endsection

    @section('script')
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{ URL::to('plugins/morris/morris.min.js')}}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ URL::to('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!--charts -->
    <script src="{{ URL::to('plugins/chartjs/Chart.min.js') }}"></script>
    <script src="{{ URL::to('js/legend.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ URL::to('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ URL::to('plugins/knob/jquery.knob.js') }}"></script>
    <!-- Itoohl dashboard demo (This is only for demo purposes) -->
    <script type="text/javascript">
           //print pies XD
      var canvas = document.getElementById('{{ $chart }}1'),
          context = canvas.getContext('2d'),
          snapshotImageElement =
             document.getElementById('snapshotImageElement'),
          loop;
      var canvas1 = document.getElementById('{{ $chart }}2'),
          context1 = canvas1.getContext('2d'),
          snapshotImageElement1 =
             document.getElementById('snapshotImageElement1'),
          loop1;
      //print qoutes
      function printContent(el){
         var dataUrl = canvas.toDataURL();
            clearInterval(loop);
            snapshotImageElement.src = dataUrl;
            snapshotImageElement.style.display = 'inline';
            canvas.style.display = 'none';
            clearInterval(loop1);
            snapshotImageElement1.src = dataUrl;
            snapshotImageElement1.style.display = 'inline';
            canvas1.style.display = 'none';

        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
        window.location.reload();
      }
   </script>
   @if( $chart == 'pieChart' )
   @include('includes.data-reports-compare-result')
   @else
   @include('includes.data-reports-compare-bar-chart')
   @endif

    @endsection
