@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Inventory List
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
   Inventory List
@endsection

@section('content')
	   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if( !$portals->isEmpty() )
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table  id="inventory-list" class="table table-bordered table-striped">
                <thead>
                <tr class="bg-blue">
                  <th>Supplier</th>
                  <th>Format</th>
                  <th>Name</th>
                  <th>Location</th>
                  <th>Industry</th>
                  @if(Auth::user()->role != 3)
                   <th>Action</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @foreach( $portals as $portal)
                  <tr>
                    <td>{{ $portal->supplier }}</td>
                    <td>{{ $portal->format }}</td>
                    <td>{{ $portal->name }}</td>
                    <td>{{ $portal->city }} {{ $portal->street_address }}</td>
                    <td>{{ $portal->industry }}</td>
                    @if(Auth::user()->role != 3)
                      <td class="text-center">
                      <a href="{{ URL::to('inventory-list/' . $portal->id ) }}"><i class="fa fa-edit"></i></a>
                      <a href="{{ URL::to('inventory-delete/' . $portal->id ) }}" class="delete-inventory"><i class="fa fa-trash"></i></a>
                    </td>
                    @endif
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          @else
          <div class="box">
            <div class="box-body">
              <h1 class="text-center">No Records Found</h1>
            </div>
          </div>  
          @endif
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
@endsection

@section('script')

<script>
  $('.delete-inventory').on('click', function(e){

  e.preventDefault();

  var deleteInventory = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteInventory;
  });
})
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#inventory-list").DataTable({
      "sort": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

