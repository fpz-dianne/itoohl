@extends('layouts.user-dashboard-layout')


@section('title')
iTOOhL | Dashboard
@endsection

@section('style')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ URL::to('plugins/morris/morris.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ URL::to('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection

@section('dashboard-title')
Dashboard
@endsection

@section('content')

@include('includes.generate-report-type-modal')

<!-- Info boxes -->
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12">
  <a href="{{ URL::route('portal') }}" class="info-box-link">
      <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="fa fa-globe"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Portal</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </a>
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box" data-toggle="modal" data-target="#generate-report-type">
    <span class="info-box-icon bg-red"><i class="fa fa-pie-chart"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Generate Reports</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <a href="{{ URL::route('booking') }}" class="info-box-link">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Bookings</span>
      </div>
      <!-- /.info-box-content -->
    </div>
  </a>  
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <a href="{{ URL::route('user-registration') }}" class="info-box-link">
    <div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Media Planners</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
  </a>
</div>
<!-- /.col -->
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
<!-- Left col -->
<section class="col-lg-7 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
  <div class="nav-tabs-custom" id="sales">
    <div class="printonly">
      
      <i class="fa fa-inbox"></i> Sales
    </div>
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs pull-right hideprint">
      <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
      <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
      <!--<li><a onclick="printContent('sales')"><i class="fa fa-print"></i></a></li>-->
      <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
    </ul>
    <div class="tab-content no-padding">
      <!-- Morris chart - Sales -->
      <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
      <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
    </div>
  </div>
  <!-- /.nav-tabs-custom -->

  <!-- solid sales graph -->
          <div  id="sales-graph">
            <div class="printonly">
              <h1>From Itoohl.com</h1>
              <div class="print-space"></div>

            </div>
          <div class="box box-solid bg-teal-gradient">
            
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">Sales Graph</h3>

              <div class="box-tools pull-right hideprint">
                <!--<a onclick="printContent('sales-graph')"><button type="button" class="btn bg-teal btn-sm"><i class="fa fa-print"></i>
                </button></a>-->
               <!-- <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>-->
              </div>
            </div>
            <div class="box-body border-radius-none">
              <div class="chart" id="line-chart" style="height: 250px;"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-border">
              <div class="row">
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label">Mail-Orders</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label">Online</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center">
                  <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label">In-Store</div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
          </div>


    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">
          <div class="box box-default" id="browser">
            <div class="printonly">
              <h1>From Itoohl.com</h1>
              <div class="print-space"></div>
            </div>
            <div class="box-header with-border">
              <h3 class="box-title">Inventory Report</h3>

              <div class="box-tools pull-right hideprint">
                 <!--<a onclick="printContent('browser')"><button type="button" class="btn btn-box-tool"><i class="fa fa-print"></i>
                </button></a>-->
              </div>
            </div>
            <div id="browser-box-content">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas class="hideprint" id="pieChart" height="150"></canvas>
                    <img id='snapshotImageElement'/>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle text-red"></i> Apparel</li>
                    <li><i class="fa fa-circle text-green"></i> Entertainment</li>
                    <li><i class="fa fa-circle text-yellow"></i> Food</li>
                    <li><i class="fa fa-circle text-aqua"></i> Personal Care</li>
                    <li><i class="fa fa-circle text-light-blue"></i> Malls</li>
                    <li><i class="fa fa-circle text-gray"></i> Others</li>
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a>Apparel
                  <span class="pull-right text-red">38%</span></a></li>
                <li><a>Entertainment <span class="pull-right text-green">21%</span></a>
                </li>
                <li><a>Food
                  <span class="pull-right text-yellow">12%</span></a></li>
                  <li><a>Personal Care <span class="pull-right text-green">12%</span></a>
                  </li>
                  <li><a>Malls <span class="pull-right text-green">15%</span></a>
                  </li>
                  <li><a>Others <span class="pull-right text-green">3%</span></a>
                  </li>               
              </ul>
                </div>
                <!-- /.footer -->
                </div>
              </div>
              <!-- /.box -->
            </section>
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->

@endsection

@section('script')
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ URL::to('plugins/morris/morris.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::to('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!--charts -->
<script src="{{ URL::to('plugins/chartjs/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ URL::to('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ URL::to('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::to('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}} "></script>
<!-- jQuery Knob Chart -->
<script src="{{ URL::to('plugins/knob/jquery.knob.js') }}"></script>
<!-- Itoohl dashboard demo (This is only for demo purposes) -->
<script src="{{ URL::to('js/dashboard.min.js') }}"></script>


<script type="text/javascript">

</script>

@endsection
