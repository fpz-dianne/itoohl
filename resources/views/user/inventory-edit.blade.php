@extends('layouts.user-dashboard-layout')

@section('title')
iTOOhL | Inventory Edit
@endsection

@section('dashboard-title')
Edit {{ $portal->name }}
@endsection

@section('content')
<!-- Main row -->
<div>
	<form class="form-horizontal col-md-12" enctype="multipart/form-data" action="{{ URL::to('update-inventory-image/' . $portal->id) }}"  method="post">
		<div class="form-group">
			<div class="image" style="position: relative;">
				<label for="inventory-image" id="upload-design">
					<img src="#" id="file-image" width="160" height="160" style="display: none; background-color: #fff;">
					<img src="{{ URL::to('uploads/inventory-images/' . $portal->inventory_image) }}" alt="User Image" id="current-image">
				</label>
				<input type="file" name="inventory-image" class="masterTooltip input-inventory-img" id="inventory-image" title="Upload Photo">
				{{ csrf_field() }}
				<p id="file-name"></p>
			</div>
			<button type="submit" class="btn btn-yellow" id="upload-photo" style="position: relative; display: none; margin: auto;">Upload Photo</button>
				@if ($portal->inventory_image != 'inventory_image_default.jpg')
					<a href="{{ URL::to('delete-inventory-image/' . $portal->id) }}" type="submit" class="btn btn-yellow delete-inventory" id="delete-photo" style="position: relative;">Delete Photo</a>
				@endif
		</div>
	</form>
</div>
<div class="row">
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="{{ URL::to('inventory-update/' . $portal->id ) }}" method="post">
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name">Name</label>
									<input class="form-control" type="text" name="name" id="name" value="{{ $portal->name }}" placeholder="{{ $portal->name }}" required>
								</div>
								<div class="form-group">
									<label for="supplier">Supplier</label>
									<input text="supplier" name="supplier" class="form-control" id="supplier" value ="{{ $portal->supplier }}" placeholder="{{ $portal->supplier }}">
								</div>
								<div class="form-group">
									<label for="company">Company</label>
									<input text="company" name="company" class="form-control" id="company" value ="{{ $portal->company }}" placeholder="{{ $portal->company }}">
								</div>
								<div class="form-group">
									<label>Inventory Country Location</label>
									<select class="form-control select2" name="country" id="country" style="width: 100%;">
										<option value="{{$portal->country}}">{{ $portal->country }}</option>
										@foreach( $countries as $country )
										<option value="{{$country->name}}">{{ $country->name }}</option>
										@endforeach
									</select>
								</div> 
								<div class="form-group">
									<label>Inventory City Location</label>
									<select class="form-control select2" name="city" id="city" style="width: 100%;">
										<option value="{{$portal->city}}">{{ $portal->city }}</option>
										@foreach( $cities as $cities )
										<option value="{{$cities->name}}">{{ $cities->name }}</option>
										@endforeach
									</select>
								</div> 
								<div class="form-group">
									<label for="street_address">Inventory Street Address Location</label>
									<input text="street_address" name="street_address" class="form-control" id="street_address" value ="{{ $portal->street_address }}" placeholder="{{ $portal->street_address }}">
								</div>
								<div class="form-group">
									<label>Format</label>
									<select class="form-control select2" name="format" id="format" style="width: 100%;">
										<option value="{{$portal->format}}">{{ $portal->format }}</option>
										@foreach( $formats as $format )
										<option value="{{$format->name}}">{{ $format->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="product">Product</label>
									<input type="text" class="form-control" name="product" id="product" placeholder="{{$portal->products}}" value="{{$portal->products}}">
								</div>
								<div class="form-group">
									<label>Industry</label>
									<select class="form-control select2" name="industry" id="industry" style="width: 100%;">
										<option value="{{$portal->industry}}">{{ $portal->industry }}</option>
										@foreach( $industries as $industry )
										<option value="{{$industry->name}}">{{ $industry->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="landmark">Land Mark</label>
									<input text="landmark" class="form-control" id="landmark" value ="{{ $portal->landmark }}" placeholder="{{ $portal->landmark }}">
								</div>
								<div class="form-group">
									<label>Size</label>
									<select class="form-control select2" name="size" id="size" style="width: 100%;">
										<option selected disabled hidden value="{{$portal->size}}">{{ $portal->size }}</option>
										@foreach( $sizes as $size )
										<option value="{{$size->name}}">{{ $size->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Payment Terms</label>
									<input text="payment_terms" name="payment_terms" class="form-control" id="payment_terms" value ="{{ $portal->payment_terms }}" placeholder="{{ $portal->payment_terms }}">
								</div>
								<div class="form-group">
									<label>Avalability</label>
									<select class="form-control select2" name="availability" id="availability" value ="{{ $portal->availability }}" style="width: 100%;">
										<option selected disabled hidden value="{{$portal->availability}}">{{ $portal->availability }}</option>
										@foreach( $availabilities as $availability )
										<option value="{{ $availability->name }}">{{ $availability->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label>Gender</label>
									<select class="form-control select2" name="gender" id="gender" value ="{{ $portal->gender }}" style="width: 100%;">
										<option selected disabled hidden value="{{$portal->gender}}">{{ $portal->gender }}</option>
										@foreach( $genders as $gender )
										<option value="{{ $gender->name }}">{{ $gender->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label>Economic Class</label>
									<select class="form-control select2" name="economic_class" id="economic_class" value ="{{ $portal->economic_class }}" style="width: 100%;">
										<option selected disabled hidden value="{{$portal->economic_class}}">{{ $portal->economic_class }}</option>
										@foreach( $economic_classes as $economic_class )
										<option value="{{ $economic_class->name }}">{{ $economic_class->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label>Age</label>
									<select class="form-control select2" name="age" id="age" value ="{{ $portal->age }}" style="width: 100%;">
										<option selected disabled hidden value="{{$portal->age}}">{{ $portal->age }}</option>
										@foreach( $ages as $age )
										<option value="{{ $age->name }}">{{ $age->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label for="rates">Rates</label>
									<input type="number" pattern="[0-9]*" class="form-control" name="rates" id="rates">
								</div>
								<div class="form-group">
									<label for="searchmap">Map Location</label>
									<input type="text" class="form-control" id="searchmap" placeholder="Search Location">
								</div>
								<div class="form-group">
									<div id="map" style="height: 500px;"></div>
								</div>
								<!--<div class="form-group">-->
									<!--<label for="latitude">Latitude</label>-->
									<input type="hidden" class="form-control" name="latitude" id="latitude" placeholder="Latitude" value="{{ $portal->latitude }}">
								<!--</div>-->
								<!--<div class="form-group">-->
									<!--<label for="longitude">Longitude</label>-->
									<input type="hidden" class="form-control" name="longitude" id="longitude" placeholder="Longitude" value="{{ $portal->longitude }}">
								<!--</div>-->
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer col-md-12">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
						{{ csrf_field() }}
					</form>
				</div>
				<!-- /.box -->


			</div>
		</div>

	</section>
</div>
@endsection

@section('script')
<script>
	function initMap() {
		var mapDiv = document.getElementById('map');
		var map = new google.maps.Map(mapDiv, {
			center: {
				lat: 14.554505, 
				lng: 121.023867
			},
			zoom: 15
		});

		var marker = new google.maps.Marker({
			position: {
				lat: 14.554505, 
				lng: 121.023867
			},
			map: map,
			draggable: true
		});

		var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

		google.maps.event.addListener(searchBox, 'places_changed',function(){
			var places = searchBox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, places;

			for (i=0; place=places[i];i++) {
				bounds.extend(place.geometry.location);
				marker.setPosition(place.geometry.location);
			}

			map.fitBounds(bounds);
			map.setZoom(15);
		});

		google.maps.event.addListener(marker,'position_changed',function(){
			var lat = marker.getPosition().lat();
			var lng = marker.getPosition().lng();

			$('#latitude').val(lat);
			$('#longitude').val(lng);
		})

	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8hnRJwlyh4zeJ_AjxFwkAHsxBtAG0Kcs&callback=initMap&libraries=visualization,places"></script>

<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#file-image').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$('input[type=file]').change(function(){
	if ($(this).get(0).files.length != 0) {
		readURL(this);
		$('#file-image').css('display', '');
		$('#upload-photo').css('display', '');
		$('#delete-photo').css('display', 'none');
		$('img#current-image').css('display', 'none');
	} else {
		$('#file-image').css('display', 'none');
		$('#upload-photo').css('display', 'none');
		$('#delete-photo').css('display', '');
		$('img#current-image').css('display', '');
	}
});
</script>

<script>
$(document).ready(function() {
	$('form.form-horizontal').mouseover(function() {
		$('form.form-horizontal input[type=file]').addClass('input-hover');
		$('form.form-horizontal #upload-design').addClass('input-hover');
	});

	$('form.form-horizontal').mouseout(function() {
		$('form.form-horizontal input[type=file]').removeClass('input-hover');
		$('form.form-horizontal #upload-design').removeClass('input-hover');
	});
});
</script>

<script>
$('.delete-inventory').on('click', function(e){

	e.preventDefault();

	var deleteInventory = $(this).attr('href');

	swal({   
		title: "Are you sure?",
		text: "You will not be able to recover this lorem ipsum!",         
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Delete", 
		closeOnConfirm: false 
	}, 

	function(){   
		window.location.href = deleteInventory;
	});
})
</script>
@endsection


