@extends('layouts.user-dashboard-layout')

@section('title')
iTOOhL | Profile
@endsection

@section('dashboard-title')
User Profile
@endsection

@section('content')

@include('includes.user-deactivate-acc-modal')
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary"> 
        <div class="box-body box-profile">

          <img class="profile-user-img img-responsive img-circle" src="{{ URL::to('uploads/avatars/' . $user->avatar) }}" alt="User profile picture">

          <h3 class="profile-username text-center">{{ $user->first_name }} {{ $user->last_name }}</h3>

          <p class="text-muted text-center">
           @if( $user->role == 1 ) 
           Admin
           @elseif( $user->role == 2 )
           Advertiser
           @else
           Vendor
           @endif
         </p>

         <hr>

         <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

         <p class="text-muted">
          {{ $user->email }}
        </p>

        <hr>

        <strong><i class="fa fa-building margin-r-5"></i> Company</strong>

        <p class="text-muted">
          {{ $user->company }}
        </p>

        <hr>

        <strong><i class="fa fa-phone margin-r-5"></i> Phone</strong>

        <p class="text-muted">
          {{ $user->phone }}
        </p>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#activity" data-toggle="tab">My Inventory</a></li>
        <li><a href="#notifications" data-toggle="tab">Notifications</a></li>
        <li><a href="#settings" data-toggle="tab">Settings</a></li>
        <li><a href="#password-email" data-toggle="tab">Password & Email</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="activity">
          <!-- My inventory -->
          @if( !$portals->isEmpty() )
                  @foreach(array_chunk($portals->all(), 2) as $row)
                  <div class="row">
                    @foreach($row as $portal)
                      <div class="col-md-6">
                        <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user-2">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="widget-inventory-header"  style="background: url('../uploads/inventory-images/{{ $portal->inventory_image }}' )">
                                <!-- /.widget-user-image -->
                                <h3 class="widget-inventory-name">{{ $portal->name }}</h3>
                              </div>
                              <div class="box-footer no-padding">
                                <ul class="nav nav-stacked">
                                  <li><a href="#">Supplier <span class="pull-right badge bg-blue">{{ $portal->supplier }}</span></a></li>
                                  <li><a href="#">Format <span class="pull-right badge bg-blue">{{ $portal->format }}</span></a></li>
                                  <li><a href="#">Location <span class="pull-right badge bg-blue"> {{ $portal->street_address }}, {{ $portal->city }}</span></a></li>
                                  <li><a href="#">Industry <span class="pull-right badge bg-blue">{{ $portal->industry }}</span></a></li>
                                </ul>
                            </div>
                          </div>
                      </div>
                    @endforeach
                </div>
                @endforeach

                <div class="row">
                  <div class="inventory-pager">
                    {{ $portals->links() }}
                  </div>
                </div>
                
                @else
                <div class="row">
                  <div class="div-col-md-12">
                    <h1 class="text-center">No Records Found</h1>
                  </div>
                </div>
               @endif
          <!-- /.my inventory -->
        </div>

        <div class="tab-pane" id="notifications">
           <!-- Post -->
          @if( !$notifications->isEmpty() )
            @foreach($notifications as $notification)
              <div class="post">
                @include('notifications.' . snake_case(class_basename($notification->type)))
                <a href="{{ URL::to('notify-delete/' . $notification->id ) }}" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
              </div>
            @endforeach
          @else
          <div class="row">
            <div class="div-col-md-12">
              <h1 class="text-center">No Records Found</h1>
            </div>
          </div>
          @endif
        </div>

        <!-- /.tab-pane -->
        <div class="tab-pane" id="settings">
          <form class="form-horizontal" enctype="multipart/form-data" action="{{ URL::route('update-avatar') }}"  method="post">
            <div class="form-group">
              <div class="col-sm-offset-2 image" style="position: relative;">
                <label for="avatar" id="label-for-avatar">
                  <img src="#" id="file-image" class="img-circle profile-upload-avatar" width="160" height="160" style="display: none; position: absolute; background-color: #fff;">
                  <img src="{{ URL::to('uploads/avatars/' . $user->avatar) }}" class="img-circle profile-avatar" alt="User Image">
                </label>
                <input type="file" name="avatar" class="masterTooltip" id="avatar" title="Upload Photo">
                {{ csrf_field() }}
              </div>
              <div class="col-sm-offset-2 profile-btn-container" style="position: relative;">
                <button type="submit" class="btn btn-yellow" id="upload-photo" style="position: relative; display: none; margin: auto;">Upload Photo</button>
                @if ( $user->avatar != 'default.jpg')
                <a href="{{ URL::to('delete-avatar/' . $user->avatar) }}" type="submit" class="btn btn-yellow" id="delete-photo" style="position: relative;">Delete Photo</a>
                @endif
              </div>
            </div>
          </form>
          <form class="form-horizontal" action="{{ URL::route('profile.update') }}" method="post">
            <div class="form-group">
              <label for="first_name" class="col-sm-2 control-label">Name</label>

              <div class="col-sm-10">
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="{{ $user->first_name }}" value="{{ $user->first_name }}" >
              </div>
            </div>
            <div class="form-group">
              <label for="last_name" class="col-sm-2 control-label">Last Name</label>

              <div class="col-sm-10">
                <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}" placeholder="{{ $user->last_name }}">
              </div>
            </div>
            <div class="form-group">
              <label for="company" class="col-sm-2 control-label">Company</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="company" id="company" value="{{ $user->company }}" placeholder="Company">
              </div>
            </div>
            <div class="form-group">
              <label for="phone" class="col-sm-2 control-label">phone</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" name="phone" id="phone" value="{{ $user->phone }}" placeholder="Phone number">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="password-email">
          <div class="row">
            <div class="col-md-6">
              <div class="box box-has-shadow no-border">
                <div class="box-header">
                  <h3 class="box-title"><strong>Password</strong></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <p>
                    Your password protects your account.
                  </p>
                  <br>
                  <p>
                    <strong>Note:</strong> To change these settings, you will need to confirm your password.
                  </p>
                  <a href="{{ URL::to('change-password/' . $user->remember_token ) }}" class="change-password">
                    <p>
                     Password
                     <span class="pull-right">Last changed: {{ $user->created_at->format('d M Y') }} <i class="fa fa-angle-right margin-l-5"></i></span>
                   </p>
                 </a>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
           <!--<div class="col-md-6">
            <div class="box box-has-shadow no-border">
              <div class="box-header">
                <h3 class="box-title"><strong>Email</strong></h3>
              </div>
              
              <div class="box-body">
                <p>
                  Your password protects your account.
                </p>
                <br>
                <p>
                  <strong>Note:</strong> To change these settings, you will need to confirm your password.
                </p>
                <a href="{{ URL::to('change-email/' . $user->remember_token ) }}" class="change-password">
                  <p>
                   Email
                   <span class="pull-right">Last changed: {{ $user->created_at->format('d M Y') }} <i class="fa fa-angle-right margin-l-5"></i></span>
                 </p>
               </a>
             </div>
             
           </div>
           
         </div> -->
         <div class="col-md-6">
          <div class="box box-has-shadow no-border">
            <div class="box-header">
              <h3 class="box-title"><strong>Deactivate my account</strong></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <p>
                  Your password protects your account.
                </p>
                <br>
                <p>
                  <strong>Note:</strong> To change these settings, you will need to confirm your password.
                </p>
              <a href="javascript::void(0)" id="deactivate-accnt" class="change-password">
                <p>
                 Deactivate
                 <span class="pull-right">{{ $user->email }}<i class="fa fa-angle-right margin-l-5"></i></span>
               </p>
             </a>
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->
       </div>
       </div>
   </div>
   <!-- /.tab-pane -->
 </div>
 <!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->

</section>

@endsection

@section('script')
<!-- Sweet Alert -->
<script src="{{ URL::to('plugins/sweet-alert/sweetalert.min.js') }}"></script>
@include('sweet::alert')
<script>
  $('input[type=file]').change(function() {
   var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
   $('#file-name').html(filename);
 });
</script>

<script>
  function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
     $('#file-image').attr('src', e.target.result);
   }
   reader.readAsDataURL(input.files[0]);
 }
}

$('input[type=file]').change(function(){
	if ($(this).get(0).files.length != 0) {
		readURL(this);
		$('#file-image').css('display', '');
		$('#upload-photo').css('display', '');
		$('#delete-photo').css('display', 'none');
	} else {
		$('#file-image').css('display', 'none');
		$('#upload-photo').css('display', 'none');
		$('#delete-photo').css('display', '');
	}
});
</script>

<script>
  $('.delete-inventory').on('click', function(e){

   e.preventDefault();

   var deleteInventory = $(this).attr('href');

   swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 

  function(){   
    window.location.href = deleteInventory;
  });
 });

  $('#deactivate-accnt').on('click', function(e){

   e.preventDefault();

   swal({
      title: "Deactivate Account",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      inputPlaceholder: "Write Something"
    },
    function(inputValue){
      if (inputValue === false) return false;
      
      if (inputValue === "") {
        swal.showInputError("You need to write something!");
        return false
      }
      
      swal("Request Submitted");
    });

  }); 
</script>

<script>
@if ($errors->any())
  sweetAlert({
    title: "Error!",
    text: "@foreach($errors->all() as $error) {{ $error }}\n @endforeach",
    type: "error",
    confirmButtonColor: "#DD6B55"
  });
@endif
</script>
@endsection