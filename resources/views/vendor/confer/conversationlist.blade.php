<h2>Recent / New Message</h2>
<ul class="confer-conversation-list">
@if ($conversations->isEmpty())
<p>No conversations have been started yet. Why not be the first?</p>
@else
@foreach ($conversations as $conversation)
	<li data-conversationId="{{ $conversation->id }}" @if($conversation->is_private) data-userId="{{ $conversation->participants()->ignoreMe()->first()->id }}" @endif>
	<h3>{{ $conversation->first_name }}</h3>
	@if ($conversation->messages->last()->type === 'user_message')
	<div class="pull-left">
		<img class="img-circle" src="{{ url('/') . config('confer.avatar_dir') . $conversation->messages->last()->sender->avatar }}">
	</div>
	<span class="confer-bar-user-message">
		{{ $conversation->messages->last()->sender->first_name }}
		<small class="confer-bar-timestamp"><i class="fa fa-clock-o"></i> {{ $conversation->messages->last()->created_at->diffForHumans() }}</small>
	</span>
	<p>{{{ $conversation->messages->last()->body }}}</p>
	@else
	<span class="confer-bar-conversation-message">
		{!! $conversation->messages->last()->body !!}
	</span>
	@endif
	</li>
@endforeach
@endif
</ul>