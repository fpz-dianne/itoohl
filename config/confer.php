<?php

return [
	'company_avatar' => '/vendor/confer/img/avatars/confer-default-global.png',
	'loader' => '/vendor/confer/img/puff.svg',
	'avatar_dir' => '/uploads/avatars/',
	'allow_global' => false,
	'enable_emoji' => false,
	'grammar_enforcer' => true
];