<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('user_email');
            $table->string('user_first_name');
            $table->integer('receiver_id');
            $table->string('receiver_email');
            $table->string('receiver_name');
            $table->string('name');
            $table->string('inventory_img');
            $table->string('placement_duration');
            $table->string('material_changes');
            $table->string('production');
            $table->string('instruction');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_quotations');
    }
}
