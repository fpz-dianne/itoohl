<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('supplier');
            $table->string('country');
            $table->string('city');
            $table->string('street_address');
            $table->double('latitude', 20,10);
            $table->double('longitude', 20,10);
            $table->string('format');
            $table->string('products');
            $table->string('industry');
            $table->string('landmark');
            $table->string('description');
            $table->string('size');
            $table->string('type');
            $table->tinyInteger('illumination');    
            $table->string('availability')->default('Yes');
            $table->string('payment_terms');
            $table->string('rates');
            $table->string('gender');
            $table->string('economic_class');
            $table->string('age');
            $table->string('profile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portals');
    }
}
