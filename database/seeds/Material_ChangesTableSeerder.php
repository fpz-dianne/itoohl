<?php

use Illuminate\Database\Seeder;

class Material_ChangesTableSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $material_change = new \App\materialchanges([
        	'name' => 'One time placement',
        ]);
        $material_change->save();

        $material_change = new \App\materialchanges([
        	'name' => 'Twice placement',
        ]);
        $material_change->save();

        $material_change = new \App\materialchanges([
        	'name' => 'Three or more placement',
        ]);
        $material_change->save();
    }
}
