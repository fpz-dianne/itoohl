<?php

use Illuminate\Database\Seeder;

class Economic_ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $economic_class = new \App\EconomicClass([
            'name' => 'A-B',
        ]);
        $economic_class->save();

        $economic_class = new \App\EconomicClass([
            'name' => 'C-D',
        ]);
        $economic_class->save();

         $economic_class = new \App\EconomicClass([
            'name' => 'E-F',
        ]);
        $economic_class->save();
    }
}
