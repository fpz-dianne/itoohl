<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new \App\country([
        	'name' => 'Philippines',
        ]);
        $country->save();

        $country = new \App\country([
        	'name' => 'United States',
        ]);
        $country->save();

        $country = new \App\country([
        	'name' => 'Europe',
        ]);
        $country->save();

        $country = new \App\country([
        	'name' => 'Japan',
        ]);
        $country->save();

        $country = new \App\country([
        	'name' => 'Indonisia',
        ]);
        $country->save();
    }
}
