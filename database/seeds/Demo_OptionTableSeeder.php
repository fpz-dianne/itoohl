<?php

use Illuminate\Database\Seeder;

class Demo_OptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demo_option = new \App\DemoOption([
        	'name' => 'Search Functionality',
        ]);
        $demo_option->save();

        $demo_option = new \App\DemoOption([
        	'name' => 'Media Planning',
        ]);
        $demo_option->save();

        $demo_option = new \App\DemoOption([
        	'name' => 'Research, Insights and Report Generation',
        ]);
        $demo_option->save();

        $demo_option = new \App\DemoOption([
        	'name' => 'All of the above',
        ]);
        $demo_option->save();
    }
}
