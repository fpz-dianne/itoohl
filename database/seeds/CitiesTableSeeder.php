<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $city = new \App\city([
            'name' => 'NLEX',
        ]);
        $city->save();

        $city = new \App\city([
            'name' => 'SLEX',
        ]);
        $city->save();

        $city = new \App\city([
            'name' => 'EDSA',
        ]);
        $city->save();

        $city = new \App\city([
            'name' => 'C5',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Alaminos',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Angeles',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Antipolo',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bacolod',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bacoor',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bago',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Baguio',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bais',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Balanga',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Batac',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Batangas City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bayawan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Baybay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bayugan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Biñan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bislig',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Bogo',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Borongan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Butuan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cabadbaran',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cabanatuan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cabuyao',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cadiz',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cagayan de Oro',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Calamba',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Calapan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Calbayog',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Caloocan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Candon',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Canlaon',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Carcar',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Catbalogan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cauayan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cavite City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cebu City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Cotabato City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Dagupan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Danao',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Dapitan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Dasmariñas',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Davao City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Digos',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Dipolog',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Dumaguete',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'El Salvador',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Escalante',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Gapan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'General Santos',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'General Trias',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Gingoog',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Guihulngan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Himamaylan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Ilagan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Iligan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Iloilo City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Imus',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Iriga',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Isabela',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Kabankalan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Kidapawan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Koronadal',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'La Carlota',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Lamitan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Laoag',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Lapu-Lapu',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Las Piñas',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Legazpi',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Ligao',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Lipa',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Lucena',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Maasin',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Mabalacat',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Makati',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Malabon',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Malaybalay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Malolos',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Mandaluyong',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Mandaue',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Manila',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Marawi',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Marikina',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Masbate City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Mati',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Meycauayan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Muñoz',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Muntinlupa',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Naga',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Navotas',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Olongapo',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Ormoc',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Oroquieta',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Ozamiz',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Pagadian',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Palayan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Panabo',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Parañaque',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Pasay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Pasig',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Passi',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Puerto Princesa',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Quezon City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Roxas',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Sagay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Samal',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Carlos',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Fernando',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Jose',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Jose del Monte',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Juan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Pablo',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'San Pedro',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Santa Rosa',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Santiago',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Silay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Sipalay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Sorsogon City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Surigao City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tabaco',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tabuk',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tacloban',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tacurong',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tagaytay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tagbilaran',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Taguig',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tagum',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Talisay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tanauan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tandag',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tangub',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tanjay',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tarlac City',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tayabas',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Toledo',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Trece Martires',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Tuguegarao',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Urdaneta',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Valencia',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Valenzuela',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Victorias',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Vigan',
        ]);
        $city->save();

        $city = new \App\city([
        	'name' => 'Zamboanga City',
        ]);
        $city->save();
    }
}
