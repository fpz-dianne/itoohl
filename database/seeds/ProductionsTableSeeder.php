<?php

use Illuminate\Database\Seeder;

class ProductionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $production = new \App\production([
        	'name' => 'Provided by advertiser',
        ]);
        $production->save();

        $production = new \App\production([
        	'name' => 'Inclusive in quotation',
        ]);
        $production->save();
    }
}
