<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PortalTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(FormatsTableSeeder::class);
        $this->call(IndustriesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(SizesTableSeeder::class);
        $this->call(Material_ChangesTableSeerder::class);
        $this->call(ProductionsTableSeeder::class);
        $this->call(AvailabilitiesTableSeeder::class);
        $this->call(Demo_OptionTableSeeder::class);
        $this->call(Demo_Option2TableSeeder::class);
        $this->call(GenderTableSeeder::class);
        $this->call(AgeTableSeeder::class);
        $this->call(Economic_ClassTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(ConferTableSeeder::class);
    }
}
