<?php

use Illuminate\Database\Seeder;

class ConferTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
  		$this->call('ConferSeeder');
    }
}
