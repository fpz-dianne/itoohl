<?php

use Illuminate\Database\Seeder;

class PortalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //Robert Test Inventory
        /*$portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 1',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 1',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.645369,
            'longitude' => 121.028068,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 2',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 1',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.647045,
            'longitude' => 121.023508,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 3',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 1',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.657383,
            'longitude' => 121.009620,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 4',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 1',
            'age' => '25-34',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.667365,
            'longitude' => 120.982682,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 5',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 1',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.692013,
            'longitude' => 120.972260,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 6',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 1',
            'age' => '25-34',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.697521,
            'longitude' => 121.053434,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 7',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Product 2',
            'age' => '18-24',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.699617,
            'longitude' => 121.053606,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 8',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Product 2',
            'age' => '25-34',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.696504,
            'longitude' => 121.032384,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 9',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Product 2',
            'age' => '25-34',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.696504,
            'longitude' => 121.032384,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 10',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Product 2',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.696504,
            'longitude' => 121.032384,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 11',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Product 2',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.613261,
            'longitude' => 121.035125,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 12',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Product 2',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.611534,
            'longitude' => 121.029039,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 13',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Product 3',
            'age' => '35-44',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.575869,
            'longitude' => 121.079732,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 14',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Product 3',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.618134,
            'longitude' => 121.020955,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 15',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Product 3',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.619089,
            'longitude' => 121.015440,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 16',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Product 3',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.639993,
            'longitude' =>  120.975550,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 17',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Product 3',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.56347,
            'longitude' => 121.04227,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 18',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Product 3',
            'age' => '55-64',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.653979,
            'longitude' =>  121.026865,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 19',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Producst',
            'age' => '55-64',
            'economic_class' => 'C-D',
            'latitude' => 14.656180,
            'longitude' =>  121.027251,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 20',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Producst',
            'age' => '55-64',
            'economic_class' => 'C-D',
            'latitude' => 14.657311,
            'longitude' => 121.022337,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 21',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Producst',
            'age' => '55-64',
            'economic_class' => 'C-D',
            'latitude' => 14.668587,
            'longitude' => 121.063276,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 22',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Producst',
            'age' => '55-64',
            'economic_class' => 'C-D',
            'latitude' => 14.672240,
            'longitude' => 121.043792,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 23',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Producst',
            'age' => '65+',
            'economic_class' => 'C-D',
            'latitude' => 14.56347,
            'longitude' => 121.04227,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 24',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Producst',
            'age' => '65+',
            'economic_class' => 'E-F',
            'latitude' => 14.654100,
            'longitude' => 121.024342,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 25',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '65+',
            'economic_class' => 'E-F',
            'latitude' => 14.657817,
            'longitude' => 121.016445,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 26',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '65+',
            'economic_class' => 'E-F',
            'latitude' => 14.673012,
            'longitude' => 120.986919,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 27',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '65+',
            'economic_class' => 'E-F',
            'latitude' => 14.634346,
            'longitude' => 121.002244,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 28',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '65+',
            'economic_class' => 'E-F',
            'latitude' => 114.633588,
            'longitude' => 121.005881,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 29',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.621417,
            'longitude' => 121.028147,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 30',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.621417,
            'longitude' => 121.028147,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 31',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.579040,
            'longitude' => 121.063763,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 32',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Producst',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.583442,
            'longitude' => 121.057905,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 33',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Others',
            'products' => 'Test Producst',
            'age' => '35-44',
            'economic_class' => 'E-F',
            'latitude' => 14.589485,
            'longitude' => 121.057390,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 3,
            'name' => 'Test Inventory 34',
            'supplier' => 'fourpointzero',
            'company' => 'FourPoint.Zero, Inc.',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Test Streets',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Others',
            'products' => 'Test Producst',
            'age' => '35-44',
            'economic_class' => 'E-F',
            'latitude' => 14.596774,
            'longitude' => 121.059750,
        ]);
        $portal->save();
        
        

         //Adcity Test Inventory
        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0001 Edsa NB Guadalupe Right - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image001.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe EDSA',
            'size' => '40 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Star Mall',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.5634768,
            'longitude' => 121.0422774,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0002 Edsa SB Boni A - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image002.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Boni Ave.',
            'landmark' => 'Boni Ave.',
            'size' => '40 x 60',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Marcus Phoenix',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.573480,
            'longitude' => 121.0459893,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0003 Edsa SB Boni B - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image003.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe EDSA SB',
            'landmark' => 'Boni Ave.',
            'size' => '40 x 60',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.563336,
            'longitude' => 121.0421933,
        ]);
        $portal->save();
        
        

         $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0004 Edsa SB Boni C - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image004.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe EDSA SB',
            'landmark' => 'Boni Ave.',
            'size' => '20 x 58',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Kanye West',
            'age' => '18-24',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.574825,
            'longitude' => 121.049106,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0005 Edsa SB Boni D - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image005.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe EDSA SB',
            'landmark' => 'Boni Ave.',
            'size' => '20 x 116',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Test Products',
            'age' => '25-34',
            'profile' => 'Coffe Drinker',
            'latitude' => 14.574725,
            'longitude' => 121.049027,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0006 Paco Quirino A - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image006.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe EDSA SB',
            'landmark' => 'T Junction SLEX/Quirino',
            'size' => '40 x 40',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Test Products',
            'age' => '25-34',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.579816,
            'longitude' => 120.998892,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0007 Edsa GA Tower SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image007.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Manila',
            'street_address' => 'T Junction SLEX/Quirino',
            'landmark' => 'T Junction SLEX/Quirino',
            'size' => '40 x 40',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '25-34',
            'profile' => 'Coffe Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.5741472,
            'longitude' => 121.0458043,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0007 Edsa GA TowerSB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image008.jpg',
            'format' => 'Wall Murals',
            'country' => 'Philippines',
            'city' => 'Manila',
            'street_address' => 'Quirino Avenue',
            'landmark' => 'GA Tower/ MRT Boni Station',
            'size' => '225 x 84',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '25-34',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.5742536,
            'longitude' => 121.0470971,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0008 Edsa BGC Kalayaan A - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image009.jpg',
            'format' => 'Wall Murals',
            'country' => 'Philippines',
            'city' => 'Manila',
            'street_address' => 'Quirino Avenue',
            'landmark' => 'GA Tower/ MRT Boni Station',
            'size' => '17 x 22',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.5630994,
            'longitude' => 121.0349582,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0009 Edsa BGC Kalayaan B - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image010.jpg',
            'format' => 'Wall Murals',
            'country' => 'Philippines',
            'city' => 'Taguig',
            'street_address' => 'Quirino Avenue',
            'landmark' => 'GA Tower/ MRT Boni Station',
            'size' => '12 x 19',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Test Products',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.562954,
            'longitude' => 121.037125,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0010 BGC 32nd Avenue 2 - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image011.jpg',
            'format' => 'Wall Murals',
            'country' => 'Philippines',
            'city' => 'Taguig',
            'street_address' => '32nd Avenue BGC',
            'landmark' => 'Exiting BGC towards EDSA',
            'size' => '12 x 40',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Test Products',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'A-B',
            'latitude' => 14.553515,
            'longitude' => 121.0486528,
        ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0011 Boni Tower Circle - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image012.jpg',
            'format' => 'Wall Murals',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Boni Avenue',
            'landmark' => 'Boni Tower',
            'size' => '98 x 39',
            'availability' => 'No',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Test Products',
            'age' => '35-44',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'C-D',
            'latitude' => 14.5761957,
            'longitude' => 121.0327399,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0012 Masinag A Cubao Bound - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image013.jpg',
            'format' => 'LCD',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Marcos/Sumulong',
            'landmark' => 'Marcos/Sumulong Junction',
            'size' => '20 x 30',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Products',
            'age' => '45-54',
            'profile' => 'Alcohol Drinker',
            'economic_class' => 'C-D',
            'latitude' => 14.6252222,
            'longitude' => 121.1181963,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0013 Masinag B Antipolo Bound - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image014.jpg',
            'format' => 'LCD',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Marcos/Sumulong',
            'landmark' => 'Marcos/Sumulong Junction',
            'size' => '20 x 19',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Test Products',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.624314,
            'longitude' => 121.119312,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0014 Masinag C Masinag Bound - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image015.jpg',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Marcos/Sumulong',
            'landmark' => 'Marcos/Sumulong Junction',
            'size' => '20 x 30',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Products',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.625228,
            'longitude' => 121.123453,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0015 Masinag D Marikina Bound - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image015.jpg',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Marcos/Sumulong',
            'landmark' => 'Marcos/Sumulong Junction',
            'size' => '20 x 19',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Test Products',
            'age' => '45-54',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.624854,
            'longitude' => 121.121780,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0016 NLEX Malinta 4 NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image016.jpg',
            'format' => 'Street Billboard',
            'country' => 'Philippines',
            'city' => 'Malinta',
            'street_address' => 'NLEX Malinta',
            'size' => '40 x 50',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Balloon Festival',
            'age' => '55-64',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.700197,
            'longitude' => 120.997260,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0017 NLEX Malinta 4 SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image017.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Malinta',
            'street_address' => 'NLEX Malinta',
            'size' => '40 x 50',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Products',
            'age' => '55-64',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.700270,
            'longitude' => 120.997270,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0018 NLEX Marilao NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image018.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Marilao',
            'street_address' => 'NLEX Marilao',
            'size' => '20 x 50',
            'rates' => '',
            'industry' => 'Others',
            'products' => 'Test Products',
            'age' => '55-64',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.769672,
            'longitude' => 120.960039,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0019 NLEX San Simon 1 NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image019.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Simon',
            'street_address' => 'NLEX San Simon',
            'size' => '20 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '55-64',
            'profile' => 'Smoker',
            'economic_class' => 'C-D',
            'latitude' => 14.996977,
            'longitude' => 120.746119,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0020 NLEX San Simon 2, NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image020.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Simon',
            'street_address' => 'NLEX San Simon',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '65+',
            'economic_class' => 'C-D',
            'latitude' => 14.997215,
            'longitude' => 120.745701,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0021 NLEX San Simon 2, SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image022.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Simon',
            'street_address' => 'NLEX San Simon',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '65+',
            'economic_class' => 'C-D',
            'latitude' => 14.994976,
            'longitude' => 120.747096,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0022 NLEX Tarlac, NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image022.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Tarlac',
            'street_address' => 'NLEX Tarlac',
            'size' => '40 x 50',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Products',
            'age' => '65+',
            'latitude' => 14.936307,
            'longitude' => 120.792794,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0023 SLEX Alabang NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image023.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Muntinlupa',
            'street_address' => 'SLEX Alabang',
            'size' => '60 x 62.5',
            'rates' => '',
            'industry' => 'Mall',
            'products' => 'Test Products',
            'age' => '65+',
            'economic_class' => 'E-F',
            'latitude' => 14.422429,
            'longitude' => 121.045603,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0024 SLEX Alabang SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image024.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Muntinlupa',
            'street_address' => 'SLEX Alabang',
            'size' => '60 x 62.5',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Test Products',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.422278,
            'longitude' => 121.045415,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0025 14.360910 A NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image025.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Cavite',
            'street_address' => 'SLEX Southwoods ',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Test Products',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.360910,
            'longitude' => 121.044867,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0026 SLEX Southwoods B NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image023.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Cavite',
            'street_address' => 'SLEX Southwoods ',
            'size' => '15 x 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Test Products',
            'age' => '45-54',
            'economic_class' => 'E-F',
            'latitude' => 14.361741,
            'longitude' => 121.044127,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0027 SLEX Susana Heights A NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image024.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Muntinlupa',
            'street_address' => 'SLEX Susana Heights',
            'size' => '15 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '55-64',
            'economic_class' => 'E-F',
            'latitude' => 14.375324,
            'longitude' => 121.041852,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0028 SLEX Susana Heights B NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image025.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Muntinlupa',
            'street_address' => 'SLEX Susana Heights',
            'size' => '15 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '55-64',
            'economic_class' => 'E-F',
            'latitude' => 14.382017,
            'longitude' => 121.039292,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0029 SLEX Sucat NB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image026.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Parañaque',
            'street_address' => 'SLEX Sucat',
            'size' => '15 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Test Products',
            'age' => '55-64',
            'economic_class' => 'E-F',
            'latitude' => 14.378854,
            'longitude' => 121.041203,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0030 SLEX Sucat A SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image027.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Parañaque',
            'street_address' => 'SLEX Sucat',
            'size' => '120 x 50',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Pepsi',
            'age' => '55-64',
            'economic_class' => 'E-F',
            'latitude' => 14.454448,
            'longitude' => 121.045050,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0031 SLEX Sucat B SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image028.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Parañaque',
            'street_address' => 'SLEX Sucat',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'H&M',
            'age' => '55-64',
            'economic_class' => 'E-F',
            'latitude' => 14.453690,
            'longitude' => 121.045372,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0032 SLEX Sucat C SB - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image029.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Parañaque',
            'street_address' => 'SLEX Sucat',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Huawei',
            'age' => '35-44',
            'economic_class' => 'E-F',
            'latitude' => 14.455331,
            'longitude' => 121.045233,
        ]);
        $portal->save();
        
        

       $portal = new \App\Portal([
            'user_id' => 2,
            'name' => '0033 Tagaytay Intersection - Carranz',
            'supplier' => 'McGraphic Carranz',
            'company' => 'McGraphic',
            'inventory_image' => 'image030.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Laguna',
            'street_address' => 'Sta Rosa Highway Tagaytay',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Brittany',
            'age' => '35-44',
            'economic_class' => 'E-F',
            'latitude' => 14.364315,
            'longitude' => 121.043707,
        ]);
        $portal->save();
        
        

        //test
         $portal = new \App\Portal([
            'user_id' => 2,
            'name' => 'Test123',
            'supplier' => 'Dummy Inc.',
            'company' => 'Dummy',
            'inventory_image' => 'image030.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Laguna',
            'street_address' => 'Sta Rosa Highway Tagaytay',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Brittany',
            'age' => '35-44',
            'economic_class' => 'E-F',
            'latitude' => 14.364315,
            'longitude' => 121.043707,
        ]);
        $portal->save();
        
        

         $portal = new \App\Portal([
            'user_id' => 2,
            'name' => 'Test113324',
            'supplier' => 'Dummy Inc.',
            'company' => 'Dummy',
            'inventory_image' => 'image030.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Laguna',
            'street_address' => 'Sta Rosa Highway Tagaytay',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Brittany',
            'age' => '35-44',
            'economic_class' => 'E-F',
            'latitude' => 14.364315,
            'longitude' => 121.043707,
        ]);
        $portal->save();
        
        
        */


        //start of actual inputs
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00001 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '1-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '20 x 15',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Villarica',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657325, 
            'longitude' => 120.984310,
        ]);
        $portal->save();
        
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00002 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '2-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '25 x 20',
            'rates' => '',
            'industry' => 'Furnitures',
            'products' => 'Zooey',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657456,
            'longitude' => 120.984178,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00003 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '3-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '25 x 15',
            'rates' => '',
            'industry' => 'Furnitures',
            'products' => 'Zooey',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657431,
            'longitude' => 120.984211,
            ]);
        $portal->save();
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00004 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '4-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Furnitures',
            'products' => 'J&T Shoe Cabinet',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657431,
            'longitude' => 120.984211,
            ]);
        $portal->save();
        
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00005 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '5-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Furnitures',
            'products' => 'J&T Kitchen Cabinet',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657431,
            'longitude' => 120.984211,
            ]);
        $portal->save();
        
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00006 MR',
            'supplier' => '',
            'company' => 'CDO',
            'inventory_image' => '6-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '35 x 38',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Holiday Ham',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657431,
            'longitude' => 120.984211,
            ]);
        $portal->save();
        
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00007 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '7-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '35 x 40',
            'rates' => '',
            'industry' => 'Automobile Accesories',
            'products' => 'Continental Tires',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657431,
            'longitude' => 120.984211,
            ]);
        $portal->save();
        
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00008 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '8-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657417,
            'longitude' => 120.983565,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 00009 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '9-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '35 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657417,
            'longitude' => 120.983565,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000010 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '10-monumentorotunda.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '15 x 20',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000011 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '11-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '30 x 25',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000012 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '12-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '25 x 45',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000013 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '13-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '30 x 25',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000014 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '14-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '30 x 25',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000015 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '15-monumentorotunda.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '15 x 20',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000016 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '16-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '35 x 30',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657536,
            'longitude' => 120.983702,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000017 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '17-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '35 x 30',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656038,
            'longitude' => 120.984470,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000018 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '18-monumentorotunda.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656038,
            'longitude' => 120.984470,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000019 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '19-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '15 x 20',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656038,
            'longitude' => 120.984470,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000020 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '20-monumentorotunda.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '15 x 20',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656686,
            'longitude' => 120.984311,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000021 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '21-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'supplier' => '',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Snow Caps',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656686,
            'longitude' => 120.984311,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000022 MR',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '22-monumentorotunda.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Caloocan',
            'street_address' => 'Monumento Rotunda',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Seetrus',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656686,
            'longitude' => 120.985360,
            ]);
        $portal->save();
        
        

        //END OF EDSA MAPPING - MONUMENTO Rotunda

        //Start of Monumento to Munos
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000023 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '23-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Top-O',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656986,
            'longitude' => 120.985360,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000024 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '24-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Lancaster',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.656986,
            'longitude' => 120.985360,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000025 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '25-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'QSR (quick service restaurant)',
            'products' => 'King Sisig',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657188,
            'longitude' => 120.985676,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000026 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '26-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657344,
            'longitude' => 120.985815,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000027 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '27-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '70 x 50',
            'rates' => '',
            'industry' => 'Furnitures',
            'products' => 'Orocan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657001,
            'longitude' => 120.987451,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000028 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '28-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657059,
            'longitude' => 120.988352,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000029 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '29-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '45 x 45',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657049,
            'longitude' => 120.989039,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000030 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '30-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '120 x 220',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bench',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657033,
            'longitude' => 120.989929,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000031 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '31-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '80 x 40',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Ayala Vertis North',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657049,
            'longitude' => 120.990546,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000032 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '32-monumentotomunoz.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '20 x 25',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657054, 
            'longitude' => 120.991528,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000033 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '33-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Picnic Hotdogs',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657080,
            'longitude' => 120.991667,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000034 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '34-monumentotomunoz.jpg',
            'format' => 'Market Privilege Panel',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '20 x 150',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Pampangas Best',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657070,
            'longitude' => 120.992144,
            ]);
        $portal->save();
        
        

//01-23-2017 new inputs

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000035 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '35-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Mega Sardines',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657091,
            'longitude' => 120.992857,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000036 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '36-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '70 x 70',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Penshoppe',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657127,
            'longitude' => 120.994316,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000037 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '37-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'RC Cola',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657143,
            'longitude' => 120.995303,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000038 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '38-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '80 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Guess',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657164,
            'longitude' => 120.996505,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000039 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '39-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'San Mig Light',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657190,
            'longitude' => 120.997058,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000040 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '40-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Jollibee Chicken Joy',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657174,
            'longitude' => 120.99790,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000041 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '41-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',    
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Be Hop',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657205,
            'longitude' => 120.99863,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000042 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '42-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Ideal Spaghetti',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657122,
            'longitude' => 121.022013,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000043 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '43-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Dickies',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657752,
            'longitude' => 121.021911,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000044 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '44-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.653737,
            'longitude' => 121.030429,
            ]);
        $portal->save();
        


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000045 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '45-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '110 x 70',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.653457,
            'longitude' => 121.031234,
            ]);
        $portal->save();
        


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000046 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '46-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '30 x 25',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Osh Kosh',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.652855,
            'longitude' => 121.031620,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000047 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '47-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '25 x 30',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Avon',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.652284,
            'longitude' => 121.032028,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000048 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '48-munoztogma.jpg',
            'format' => 'LRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 12',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'Platinum Karaoke',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.651371,
            'longitude' => 121.032586,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000049 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '49-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'RDL Pharma',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.650862,
            'longitude' => 121.023940,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000050 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '50-munoztogma.jpg',
            'format' => 'LRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 12',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Novuhair',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.650333,
            'longitude' => 121.0333326,
            ]);
        $portal->save();
        


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000051 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '51-munoztogma.jpg',
            'format' => 'LRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 12',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Racks',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.649741,
            'longitude' => 121.0033626,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000052 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '52-munoztogma.jpg',
            'format' => 'LRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 12',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Flawless',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.649741,
            'longitude' => 121.033626,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000053 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '53-munoztogma.jpg',
            'format' => 'LRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 12',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Pioneer Insurance',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.649315,
            'longitude' => 121.033948,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000054 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '54-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Star Life SMDC',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.648640,
            'longitude' => 121.034356,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000055 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '55-munoztogma.jpg',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '8 x 100',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.657174,
            'longitude' => 120.997900,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000056 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '56-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '75 x 240',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'San Mig Light',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.648360,
            'longitude' => 121.03554,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000057 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '57-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'Spotify',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.647140,
            'longitude' => 121.03539,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000058 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '58-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Fiesta Ham',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646891,
            'longitude' => 121.03554,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000059 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '59-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Canadian Ham',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646891,
            'longitude' => 121.03554,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000060 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '60-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Halo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646891,
            'longitude' => 121.03554,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000061 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '61-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646892,
            'longitude' => 121.03555,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000062 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '62-munoztogma.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03564,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000063 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '63-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bench',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03569,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000064 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '64-munoztogma.jpg',
            'format' => 'Static Bus Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Unipak Mackerel',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03564,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000065 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '65-munoztogma.jpg',
            'format' => 'LED Billboard Gantry',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '20 x 100',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03564,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000066 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '66-munoztogma.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03564,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000067 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '67-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Petrol',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03564,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000068 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '68-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646569,
            'longitude' => 121.03564,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000069 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '69-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'MET 1',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646008,
            'longitude' => 121.03603,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000070 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '70-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'MET 1',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646008,
            'longitude' => 121.03603,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000071 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '71-monumentotomunoz.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.646008,
            'longitude' => 121.03603,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000072 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '72-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Century Bacon',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.645852,
            'longitude' => 121.03613,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000073 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '73-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Pepsi',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.645852,
            'longitude' => 121.03613,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000074 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '74-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Executive Optical',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.645530,
            'longitude' => 121.03632,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000075 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '75-munoztogma.jpg',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Mushroom Pepper Steak',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.644784,
            'longitude' => 121.03672,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000076 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '76-munoztogma.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '70 x 50',
            'rates' => '',
            'industry' => 'Services',
            'products' => '2GO Express',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.644711,
            'longitude' => 121.03698,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000077 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '77-munoztogma.jpg',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'San Miguel Flavored Beer',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.644586,
            'longitude' => 121.03706,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000078 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '78-munoztogma.jpg',
            'format' => 'Board Up',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '20 x 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'SMDC',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.643320,
            'longitude' => 121.03780,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000079 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '79-munoztogma.jpg',
            'format' => 'Board Up',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Munoz',
            'size' => '15 x 30',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Hinugot ka sa lupa',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.642438,
            'longitude' => 121.03833,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000080 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '80-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Racks Restaurant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.640609,
            'longitude' => 121.04569,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000081 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '81-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 50',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'BPI Auto Loan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.640609,
            'longitude' => 121.04569,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000082 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '82-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Twin Lakes',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.605253,
            'longitude' => 121.05741,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000083 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '83-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '70 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Cheese Ring',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.605253,
            'longitude' => 121.05741,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000084 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '84-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '80 x 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'My Slim',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.630536,
            'longitude' => 121.04572,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000085 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '85-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Chimes Greenhills',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.630536,
            'longitude' => 121.04572,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000086 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '86-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Robina Farms',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.630214,
            'longitude' => 121.04586,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000087 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '87-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Calayan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.629986,
            'longitude' => 121.04596,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000088 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '88-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.629374,
            'longitude' => 121.04637,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000089 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '89-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '50 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.629374,
            'longitude' => 121.04637,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000090 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '90-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '25 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Michaela',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.629374,
            'longitude' => 121.04637,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000091 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '91-gmatosantolan.jpg',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'San Miguel Flavored Beer',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.629374,
            'longitude' => 121.04637,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000092 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '92-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Acne Care',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.626298,
            'longitude' => 121.04773,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000093 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '93-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Assasins Creed',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.626298,
            'longitude' => 121.04773,
            ]);
        $portal->save();
        
        
// $portal = new \App\Portal([
//             'user_id' => 7,
//             'name' => 'EDSA 000093 SB',
//             'supplier' => '',
//             'company' => '',
//             'inventory_image' => '34-guadalupetoroxas.jpg',
//             'format' => 'Static Billboard',
//             'country' => 'Philippines',
//             'city' => 'Quezon City',
//             'street_address' => 'GMA',
//             'size' => '50 x 50',
//             'rates' => '',
//             'industry' => 'Entertainment',
//             'products' => 'Assasins Creed',
//             'age' => '',
//             'economic_class' => '',
//             'latitude' => 14.625483,
//             'longitude' => 121.04860,
//         ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000094 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '94-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Periwinkle Creed',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.625483,
            'longitude' => 121.04860,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000095 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '95-gmatosantolan.jpg',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Mushroom Pepper Steak',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.625337,
            'longitude' => 121.048214,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000096 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '96-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '25 x 50',
            'rates' => '',
            'industry' => 'Hardware',
            'products' => 'Hardiflex',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.624688,
            'longitude' => 121.048509,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000097 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '97-gmatosantolan.jpg',
            'format' => 'LED Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '25 x 30',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.624439,
            'longitude' => 121.048675,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000098 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '98-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bench',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.624439,
            'longitude' => 121.048675,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000099 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '99-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '25 x 125',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Victory Liner',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.623616,
            'longitude' => 121.048988,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000100 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '100-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '70 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Clothing',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.624171,
            'longitude' => 121.048795,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000101 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '101-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Tropical Hut',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.624171,
            'longitude' => 121.048796,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000102 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '102-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Office Equipment',
            'products' => 'HP Ink Tank System',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.623974,
            'longitude' => 121.048870,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000103 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '103-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chao Fan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.623974,
            'longitude' => 121.048870,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000104 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '104-gmatosantolan.jpg',
            'format' => 'MRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '9 x 3',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'David Guetta Concert',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.623553,
            'longitude' => 121.049026,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000105 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '105-gmatosantolan.jpg',
            'format' => 'MRT Central Pillar',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '9 x 3',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Brian Adams Concert',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.622930,
            'longitude' => 121.049289,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000106 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '106-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.620885,
            'longitude' => 121.050206,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000107 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '107-gmatosantolan.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '20 x 50',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.620065,
            'longitude' => 121.050786,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000108 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '108-gmatosantolan.jpg',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '8 x 100',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Master Hans',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.619634,
            'longitude' => 121.050786,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000109 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '109-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Samson',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.619406,
            'longitude' => 121.050893,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000110 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '110-gmatosantolan.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '20 x 60',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.619406,
            'longitude' => 121.050893,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000111 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '111-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 50',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.618820,
            'longitude' => 121.051660,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000112 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '112-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Grand Cirque',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.618358,
            'longitude' => 121.051360,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000113 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '113-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Orgarnique',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.618358,
            'longitude' => 121.051360,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000114 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '114-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.618358,
            'longitude' => 121.051360,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000115 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '115-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '70 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Freego',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.617662,
            'longitude' => 121.051650,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000116 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '116-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 60',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Cobra',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.617589,
            'longitude' => 121.051698,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000117 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '117-gmatosantolan.jpg',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '8 x 100',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.617335,
            'longitude' => 121.051827,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000118 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '118-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '80 x 80',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.617335,
            'longitude' => 121.051827,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000119 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '119-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '70 x 50',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Honda City',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.617024,
            'longitude' => 121.051956,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000120 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '120-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '30 x 40',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Hot Air Baloon',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.616842,
            'longitude' => 121.052069,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000121 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '121-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '35 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'American Ham',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.616624,
            'longitude' => 121.052160,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000122 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '122-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'El Real',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.616624,
            'longitude' => 121.052160,
            ]);
        $portal->save();
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000123 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '123-gmatosantolan.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'GMA',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'HOOQ',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.616624,
            'longitude' => 121.052160,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000124 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '124-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '25 x 35',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Axn Eyewear',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.6068574,
            'longitude' => 121.056616,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000125 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '125-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '30 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Great Image',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606857,
            'longitude' => 121.056616,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000126 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '126-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Santolan',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'Panasonic',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606899,
            'longitude' => 121.056696,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000127 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '127-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Belo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606811,
            'longitude' => 121.056734,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000128 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '128-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Santolan',
            'size' => '20 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Orgarnique',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606785,
            'longitude' => 121.056745,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000129 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '129-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Assasins Creed',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606759,
            'longitude' => 121.056750,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000130 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '130-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Santolan',
            'size' => '30 x 40',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'The Great Wall',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606761,
            'longitude' => 121.056752,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000131 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '131-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '30 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Belladonna',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606761,
            'longitude' => 121.056752,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000132 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '132-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Santolan',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Harvey Fresh',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606762,
            'longitude' => 121.056752,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000133 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '133-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Bangus',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606762,
            'longitude' => 121.056753,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000134 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '134-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Santolan',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606762,
            'longitude' => 121.056754,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000135 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '135-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '30 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Boardwalk',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606762,
            'longitude' => 121.056755,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000136 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '136-santolantoshaw.jpg',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Quezon City',
            'street_address' => 'Santolan',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Pale Pilsen',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.606290,
            'longitude' => 121.056948,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000137 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '137-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.605420,
            'longitude' => 121.057345,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000138 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '138-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '70 x 40',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chicken Joy',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.605373,
            'longitude' => 121.057366,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000139 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '139-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '70 x 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Belo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.601760,
            'longitude' => 121.059002,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000140 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '140-santolantoshaw.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.601760,
            'longitude' => 121.059003,
            ]);
        $portal->save();
        
        



        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000141 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '141-santolantoshaw.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.600452,
            'longitude' => 121.059372,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000142 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '142-santolantoshaw.jpg',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '8 x 100',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.599746,
            'longitude' => 121.059458,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000143 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '143-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '70 x 70',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Goldilucks',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.598599,
            'longitude' => 121.059528,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000144 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '144-santolantoshaw.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'NSP (Network Service Provider)',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.598090,
            'longitude' => 121.059633,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000145 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '145-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '50 x 120',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Porsche',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.596191,
            'longitude' => 121.059134,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000146 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '146-santolantoshaw.jpg',
            'format' => 'MTR Center BillBoard',
            'country' => 'Philippines',
            'city' => 'San Juan',
            'street_address' => 'Santolan',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Premium Beers',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.598669,
            'longitude' => 121.059000,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000147 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '147-santolantoshaw.jpg',
            'format' => 'MTR Center Copin Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '8 x 75',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Tender Juicy Hotdogs',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.595563,
            'longitude' => 121.058925,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000148 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '148-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Michaela Bags',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.595563,
            'longitude' => 121.058926,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000149 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '149-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Hairspray',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.595563,
            'longitude' => 121.058927,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000150 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '150-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '80 x 280',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'AMA Online',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.594753,
            'longitude' => 121.058700,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000151 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '151-santolantoshaw.jpg',
            'format' => 'MRT Center Copin Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '8 x 30',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Enchanted Kingdom',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.594120,
            'longitude' => 121.058534,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000151 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '151-santolantoshaw.jpg',
            'format' => 'MRT Center Copin Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '8 x 30',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Enchanted Kingdom',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.594120,
            'longitude' => 121.058534,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000152 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '152-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '90 x 70',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.593679,
            'longitude' => 121.058341,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000153 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '153-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '60 x 30',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Magnolia Chocolate',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592999,
            'longitude' => 121.058094,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000154 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '154-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '60 x 30',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Flavored Beers',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592999,
            'longitude' => 121.058095,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000155 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '155-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '60 x 30',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Premium Beers',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592999,
            'longitude' => 121.058096,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000156 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '156-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '30 x 40',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'HOOQ',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592490,
            'longitude' => 121.057933,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000157 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '157-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '50 x 60',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chicken Mcdo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592490,
            'longitude' => 121.057933,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000158 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '158-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '25 x 50',
            'rates' => '',
            'industry' => 'Hardware',
            'products' => 'Eagle Cement',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592417,
            'longitude' => 121.057933,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000159 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '159-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '10 x 75',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'San Mig Food Ave',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.592417,
            'longitude' => 121.057933,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000160 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '160-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '70 x 60',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Sunlife',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591981,
            'longitude' => 121.057777,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000161 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '161-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Snow Caps',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591981,
            'longitude' => 121.057778,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000162 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '162-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Hardware',
            'products' => 'NXLED',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591981,
            'longitude' => 121.057779,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000163 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '163-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Hardware',
            'products' => 'Akari',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591981,
            'longitude' => 121.057780,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000163 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '163-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Hardware',
            'products' => 'Akari',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591981,
            'longitude' => 121.057780,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000164 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '164-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Toys',
            'products' => 'Mattel',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591981,
            'longitude' => 121.057781,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000165 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '165-santolantoshaw.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '80 x 60',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.591285,
            'longitude' => 121.057546,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000166 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '166-santolantoshaw.jpg',
            'format' => 'LED Board Gantry',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '20 x 100',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.590299,
            'longitude' => 121.057135,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000166 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '166-santolantoshaw.jpg',
            'format' => 'LED Board Gantry',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '20 x 100',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.590299,
            'longitude' => 121.057135,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000167 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '167-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '25 x 30',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Morris Garages MG',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.598889,
            'longitude' => 121.057133,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000168 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '168-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Freego',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.589889,
            'longitude' => 121.057134,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000169 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '169-santolantoshaw.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '25 x 30',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Tanduay Ice',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.589889,
            'longitude' => 121.057135,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000170 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '170-santolantoshaw.jpg',
            'format' => 'MRT Center Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'MNL48',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.589494,
            'longitude' => 121.057058,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000171 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '171-santolantoshaw.jpg',
            'format' => 'MRT Vertical Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '9 x 3',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Novuhair',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.589286,
            'longitude' => 121.057010,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000172 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '172-santolantoshaw.jpg',
            'format' => 'MRT Vertical Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '9 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Premium Beers',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.589286,
            'longitude' => 121.057011,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000173 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '173-santolantoshaw.jpg',
            'format' => 'MRT Center Copin Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Santolan',
            'size' => '8 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Master Hans',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.589286,
            'longitude' => 121.057012,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000174 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '174-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 70',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Thiocell',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.580967,
            'longitude' => 121.052965,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000175 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '175-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Axis Condomimium',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.580469,
            'longitude' => 121.052632,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000176 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '176-shawtoguadalupe.jpg',
            'format' => 'MRT Center Copin Banner',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '8 x 30',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Honda Beat',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.580344,
            'longitude' => 121.052632,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000177 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '177-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Lea Perrins',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.580344,
            'longitude' => 121.052579,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000178 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '178-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Vitamilk',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.579430,
            'longitude' => 121.051860,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000179 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '179-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '70 x 60',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'BDO Home Loan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.577619,
            'longitude' => 121.050737,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000180 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '180-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '25 x 20',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Magnolia Ice Cream',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.577213,
            'longitude' => 121.050478,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000181 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '181-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 120',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Cobra Energy Drink',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.576761,
            'longitude' => 121.050660,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000182 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '182-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '15 x 20',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'OLGC',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.576522,
            'longitude' => 121.049909,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000183 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '183-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Eplus',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.576553,
            'longitude' => 121.049909,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000184 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '184-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '280 x 80',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Heineken',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.576491,
            'longitude' => 121.049877,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000185 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '185-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 70',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Metrobank Car Loan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.574988,
            'longitude' => 121.048854,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000186 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '186-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 70',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'St Peters Chapel',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.574988,
            'longitude' => 121.048854,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000187 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '187-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '20 x 40',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'OPM Against Drugs',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.574978,
            'longitude' => 121.048820,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000188 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '188-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '20 x 40',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'The Rail',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.574978,
            'longitude' => 121.048820,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000189 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '189-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '20 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Saucony',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.574978,
            'longitude' => 121.048820,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000190 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '190-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Argentina Bacon',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.573815,
            'longitude' => 121.047967,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000191 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '191-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '20 x 15',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Local Government',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.573067,
            'longitude' => 121.047441,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000192 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '192-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 30',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Vitamilk',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.573067,
            'longitude' => 121.047441,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000193 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '193-shawtoguadalupe.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '40 x 30',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.572511,
            'longitude' => 121.047092,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000194 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '194-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 70',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chicken Joy Perfect',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000195 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '195-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '90 x 50',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chicken Joy Bring Joy',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000196 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '196-shawtoguadalupe.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000197 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '197-shawtoguadalupe.jpg',
            'format' => 'Board Up',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '25 x 60',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Heneken',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000198 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '198-shawtoguadalupe.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '30 x 90',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000199 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '199-shawtoguadalupe.jpg',
            'format' => 'Static Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000200 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '200-shawtoguadalupe.jpg',
            'format' => 'Static Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Kuya J',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571680,
            'longitude' => 121.046716,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000201 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '201-shawtoguadalupe.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571483,
            'longitude' => 121.046629,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000202 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '202-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 70',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'The Residences of the Westin',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571483,
            'longitude' => 121.046629,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000203 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '203-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 70',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Amber',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571431,
            'longitude' => 121.046613,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000204 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '204-shawtoguadalupe.jpg',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571322,
            'longitude' => 121.046575,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000205 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '205-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '30 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Kashieca',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571322,
            'longitude' => 121.046575,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000206 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '206-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Hoo Dermatology',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571114,
            'longitude' => 121.046511,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000207 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '207-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.571114,
            'longitude' => 121.046511,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000208 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '208-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '50 x 70',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Flawless',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.570958,
            'longitude' => 121.046484,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000209 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '209-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '20 x 60',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Kenny Roger Roasters',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.570958,
            'longitude' => 121.046484,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000210 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '210-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Katinko',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.570958,
            'longitude' => 121.046484,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000211 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '211-shawtoguadalupe.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Mandaluyong',
            'street_address' => 'Shaw',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Sunnies',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.570958,
            'longitude' => 121.046484,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000212 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '212-guadalupetoroxas.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '40 x 90',
            'rates' => '',
            'industry' => 'NSP (Network Service Provider)',
            'products' => 'One Sky',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.578474,
            'longitude' => 121.045713,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000213 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '213-guadalupetoroxas.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '70 x 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Promneceum',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.578214,
            'longitude' => 121.045645,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000214 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '214-guadalupetoroxas.jpg',
            'format' => 'Static BillBoard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'Powerplate Mall',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.578214,
            'longitude' => 121.045645,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000215 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '215-guadalupetoroxas.jpg',
            'format' => 'MRT Center Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '70 x 30',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'Grab',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.5767186,
            'longitude' => 121.045355,
            ]);
        $portal->save();



        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000215 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '215-guadalupetoroxas.jpg',
            'format' => 'MRT Center Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '70 x 30',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'Grab',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.5767186,
            'longitude' => 121.045355,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000216 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '216-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '20 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Book',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.576355,
            'longitude' => 121.045140,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000217 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '217-guadalupetoroxas.jpg',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Tropicana',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.576168,
            'longitude' => 121.045097,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000218 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '218-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565618,
            'longitude' => 121.045387,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000219 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '219-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565618,
            'longitude' => 121.045387,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000220 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '220-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bayo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565327,
            'longitude' => 121.044840,
            ]);
        $portal->save();



        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000221 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '221-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565327,
            'longitude' => 121.044840,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000222 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '222-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'For Validation',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.566178,
            'longitude' => 121.045022,
            ]);
        $portal->save();



        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000223 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '223-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'For Validation',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.566178,
            'longitude' => 121.045023,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000224 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '224-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Unica Hija',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.566178,
            'longitude' => 121.045024,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000225 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '225-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '30 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Great Image',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565389,
            'longitude' => 121.044872,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000226 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '226-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Weight Gain',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565389,
            'longitude' => 121.044873,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000227 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '227-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565389,
            'longitude' => 121.044874,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000228 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '228-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565389,
            'longitude' => 121.044874,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000229 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '229-guadalupetoroxas.jpg',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '8 x 100',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.565140,
            'longitude' => 121.044775,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000230 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '230-guadalupetoroxas.jpg',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '70 x 40',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Mastercard',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.564226,
            'longitude' => 121.044914,
            ]);
        $portal->save();


// $portal = new \App\Portal([
//             'user_id' => 7,
//             'name' => 'EDSA 000230 SB',
//             'supplier' => '',
//             'company' => '',
//             'inventory_image' => '34-guadalupetoroxas.jpg',
//             'format' => 'Static Billboard',
//             'country' => 'Philippines',
//             'city' => 'Makati',
//             'street_address' => 'Guadalupe',
//             'size' => '70 x 40',
//             'rates' => '',
//             'industry' => 'Financial',
//             'products' => 'Mastercard',
//             'age' => '',
//             'economic_class' => '',
//             'latitude' => 14.564226,
//             'longitude' => 121.044914,
//         ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000231 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '231-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563868,
            'longitude' => 121.044916,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000232 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '232-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '80 x 110',
            'rates' => '',
            'industry' => 'NSP (Network Service Provider)',
            'products' => 'Smart',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563106,
            'longitude' => 121.044365,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000233 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '233-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'National Bookstore',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563419,
            'longitude' => 121.044948,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000234 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '234-guadalupetoroxas.jpg',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '40 x 30',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'Electroworld',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563418,
            'longitude' => 121.044947,
            ]);
        $portal->save();



//------------------------------------------------------------------------------------------------------------------------------------------------

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000235 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '235-guadalupetoroxas.jpg',
            //'category'='QSR (quick service restaurant)',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Mushroom Pepper Steak',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563418,
            'longitude' => 121.044948,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000236 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '236-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '150 x 150',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563418,
            'longitude' => 121.044949,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000237 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '237-guadalupetoroxas.jpg',
            //'category'='Clothing',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '90 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Jag',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563356,
            'longitude' => 121.043904,
            ]);
        $portal->save();



        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000238 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '238-guadalupetoroxas.jpg',
            //'category'='QSR (quick service restaurant)',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '80 x 200',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Mcdonals Fries',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.563356,
            'longitude' => 121.043905,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000239 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '239-guadalupetoroxas.jpg',
            //'category'='QSR (quick service restaurant)',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'San Miguel Light',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562827,
            'longitude' => 121.043540,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000240 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '240-guadalupetoroxas.jpg',
            //'category'='Religion',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 60',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Pastor Quiboloy ',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562682,
            'longitude' => 121.043422,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000241 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '241-guadalupetoroxas.jpg',
            //'category'='Spread',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '140 x 110',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Ladys Choice',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562319,
            'longitude' => 121.043744,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000242 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '242-guadalupetoroxas.jpg',
            //'category'='Beauty Product',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 50',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Snow Caps',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562475,
            'longitude' => 121.043293,
            ]);
        $portal->save();


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000243 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '243-guadalupetoroxas.jpg',
            //'category'='Clothing',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bench',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562475,
            'longitude' => 121.043293,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000244 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '244-guadalupetoroxas.jpg',
            //'category'='Clothing',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bench',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562475,
            'longitude' => 121.043293,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000245 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '245-guadalupetoroxas.jpg',
            //'category'='Vertical',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '8 x 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562475,
            'longitude' => 121.043293,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000246 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '246-guadalupetoroxas.jpg',
            //'category'='Clothing',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'SM Men Plus',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.562475,
            'longitude' => 121.043293,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000247 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '247-guadalupetoroxas.jpg',
            //'category'='Accessories',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Philip Stein',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.560465,
            'longitude' => 121.041814,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000248 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '248-guadalupetoroxas.jpg',
            //'category'='Shopping Mall',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '15 x 30',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'Rockwell Mall',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.560164,
            'longitude' => 121.040741,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000249 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '249-guadalupetoroxas.jpg',
            //'category'='Sport Accessories',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '15 x 30',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Rudy Project',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.5592520,
            'longitude' => 121.040741,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000250 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '250-guadalupetoroxas.jpg',
            //'category'='Content',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'Spotify',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.559240,
            'longitude' => 121.040398,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000251 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '251-guadalupetoroxas.jpg',
            //'category'='Vertical',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Calixto',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.557817,
            'longitude' => 121.038746,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000252 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '252-guadalupetoroxas.jpg',
            //'category'='Gas Stations',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Shell',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.557734,
            'longitude' => 121.038791,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000253 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '253-guadalupetoroxas.jpg',
            //'category'='Alcohol',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Zero',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.557184,
            'longitude' => 121.037147,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000254 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '254-guadalupetoroxas.jpg',
            //'category'='Juice',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Tropicana',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.554016,
            'longitude' => 121.033489,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000255 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '255-guadalupetoroxas.jpg',
            //'category'='Alcohol',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '10 x 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Flavored Beers',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.554016,
            'longitude' => 121.033489,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000256 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '256-guadalupetoroxas.jpg',
            //'category'='Alcohol',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '80 x 200',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Johny Walker',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.553736,
            'longitude' => 121.033489,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000257 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '257-guadalupetoroxas.jpg',
            //'category'='Vertical',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '8 x 50',
            'rates' => '',
            'industry' => 'Real Estate ',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.554182,
            'longitude' => 121.034412,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000258 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '258-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 60',
            'rates' => '',
            'industry' => '',
            'products' => ' ',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.550789,
            'longitude' => 121.030464,
            ]);
        $portal->save();
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000259 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '259-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '100 x 100',
            'rates' => '',
            'industry' => '',
            'products' => ' ',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.550952,
            'longitude' => 121.029541,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000260 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '260-guadalupetoroxas.jpg',
            //'category'='Vertical',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 80',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Coast Residences',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.550765,
            'longitude' => 121.029434,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000261 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '261-guadalupetoroxas.jpg',
            //'category'='Casino',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Okada',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.550765,
            'longitude' => 121.029434,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000262 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '262-guadalupetoroxas.jpg',
            //'category'='Foorwear',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Gibi Shoes',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.552874,
            'longitude' => 121.031611,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000263 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '263-guadalupetoroxas.jpg',
            //'category'='Foorwear',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 80',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Acnecare',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.552562,
            'longitude' => 121.031611,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000264 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '264-guadalupetoroxas.jpg',
            //'category'='Casino',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'City of Dreams',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.551877,
            'longitude' => 121.031655,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000265 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '265-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.551877,
            'longitude' => 121.031514,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000266 SB',
            'supplier' => 'Alcohol',
            'company' => '',
            'inventory_image' => '266-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.551790,
            'longitude' => 121.030742,
            ]);
        $portal->save();
        
        


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000267 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '267-guadalupetoroxas.jpg',
            //'category'='Electronics',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'Xenon',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.551375,
            'longitude' => 121.030742,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000268 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '268-guadalupetoroxas.jpg',
            //'category'='Exibit',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Hvacar',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.551738,
            'longitude' => 121.030636,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000269 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '269-guadalupetoroxas.jpg',
            //'category'='Canned',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Prime Mom',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.551738,
            'longitude' => 121.030636,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000270 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '270-guadalupetoroxas.jpg',
            //'category'='Exhibit',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Hvacar',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.548550,
            'longitude' => 121.0026987,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000271 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '271-guadalupetoroxas.jpg',
            //'category'='Malls',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Announcement',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.548550,
            'longitude' => 121.002688,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000272 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '272-guadalupetoroxas.jpg',
            //'category'='Mobile Headset',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'Gadgets',
            'products' => 'Torque',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.548550,
            'longitude' => 121.002689,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000273 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '273-guadalupetoroxas.jpg',
            //'category'='Aluminum Clad',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'Hardware',
            'products' => 'Alusign',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.548550,
            'longitude' => 121.002690,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000274 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '274-guadalupetoroxas.jpg',
            //'category'='Clothing',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 30',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'The Penthhouse',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.548550,
            'longitude' => 121.002691,
            ]);
        $portal->save();
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000275 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '275-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.548128,
            'longitude' => 121.002711,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000276 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '276-guadalupetoroxas.jpg',
            //'category'='Confectionary',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Dunkin Donuts',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.547848,
            'longitude' => 121.026923,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000277 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '277-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.547869,
            'longitude' => 121.026033,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000278 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '278-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Flor Al Mansions',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.547630,
            'longitude' => 121.025776,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000279 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '279-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 35',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'D Morlie Suites',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.547630,
            'longitude' => 121.025777,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000280 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '280-guadalupetoroxas.jpg',
            //'category'='Vertical',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 60',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Suntrust',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.547059,
            'longitude' => 121.025915,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000281 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '281-guadalupetoroxas.jpg',
            //'category'='Live Performance',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Rakrakan',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.547059,
            'longitude' => 121.025915,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000282 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '282-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '10 x 80',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Kabayan Hotel',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.546779,
            'longitude' => 121.024670,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000283 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '283-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '30 x 35',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Kabayan Hotel',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.546779,
            'longitude' => 121.024671,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000284 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '284-guadalupetoroxas.jpg',
            //'category'='Clothing',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '20 x 25',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Dickies',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.546395,
            'longitude' => 121.024155,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000285 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '285-guadalupetoroxas.jpg',
            //'category'='Banking',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '8 x 40',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Metrobank',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.545751,
            'longitude' => 121.023597,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000286 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '286-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '25 x 50',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Kamagong',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.545221,
            'longitude' => 121.023018,
            ]);
        $portal->save();
        
        


        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000287 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '287-guadalupetoroxas.jpg',
            //'category'='Casino',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '80 x 60',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Okada',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544899,
            'longitude' => 121.023286,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000288 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '288-guadalupetoroxas.jpg',
            //'category'='TV',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 25',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'LED TV',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544899,
            'longitude' => 121.023286,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000289 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '289-guadalupetoroxas.jpg',
            //'category'='QSR (quick service restaurant)',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 80',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Mang Inasal',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544951,
            'longitude' => 121.022674,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000290 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '290-guadalupetoroxas.jpg',
            //'category'='QSR (quick service restaurant)',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544349,
            'longitude' => 121.022663,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000291 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '291-guadalupetoroxas.jpg',
            //'category'='Motorcycle',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 70',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Yamaha',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544671,
            'longitude' => 121.022298,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000292 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '292-guadalupetoroxas.jpg',
            //'category'='Exhibit',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '20 x 60',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'World Pigeon Expo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544478,
            'longitude' => 121.022226,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000293 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '293-guadalupetoroxas.jpg',
            //'category'='QSR (quick service restaurant)',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chowking',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544478,
            'longitude' => 121.022226,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000294 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '294-guadalupetoroxas.jpg',
            //'category'='Alcohol',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Tanduay Ice',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544226,
            'longitude' => 121.022494,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000295 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '295-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '60 x 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544453,
            'longitude' => 121.022191,
            ]);
        $portal->save();
        
        

        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000296 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '296-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.544279,
            'longitude' => 121.021976,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000297 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '297-guadalupetoroxas.jpg',
            //'category'='',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '25 x 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543936,
            'longitude' => 121.022166,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000298 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '298-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543910,
            'longitude' => 121.022123,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000299 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '299-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '25 x 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543946,
            'longitude' => 121.021589,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000300 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '300-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '50 x 50',
            'rates' => '',
            'industry' => 'Estate',
            'products' => 'Pinoy Big Brother',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543946,
            'longitude' => 121.021589,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000301 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '301-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '40 x 40',
            'rates' => '',
            'industry' => 'Estate',
            'products' => 'Pinoy Big Brother',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543735,
            'longitude' => 121.021350,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000302 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '302-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Estate',
            'products' => 'Pinoy Big Brother',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543621,
            'longitude' => 121.021050,
            ]);
        $portal->save();
        
        
        $portal = new \App\Portal([
            'user_id' => 7,
            'name' => 'EDSA 000303 SB',
            'supplier' => '',
            'company' => '',
            'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'Guadalupe',
            'size' => '30 x 50',
            'rates' => '',
            'industry' => 'Estate',
            'products' => 'Pinoy Big Brother',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();


        //Robert input Edsa NB  306-645


       /* $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000306 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '15 X 200',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'DOUBLE DRAGON',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000307 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '25 X 25',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'DOUBLE DRAGON',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000308 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '35 X 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'GUESS',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000309 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Lee',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000310 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '25 X 25',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000311 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '25 X 30',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Agromatic',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000312 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 40',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000313 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '70 X 60',
            'rates' => '',
            'industry' => '',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000314 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '25 X 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000315 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000316 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Master Siomai',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000317 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            //'industry' => 'Food',
            'products' => 'Vacant',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000318 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '80 X 60',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'Google Duo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000319 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '100 X 40',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'WASHING MACHINE/REFRIGERATOR',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000320 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '100 X 40',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'Aircon',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000321 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 50',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000322 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 30',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Nissin Cup Noodles',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000323 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Michaela',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000324 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '80 X 40',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Jollibee',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000325 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '80 X 40',
            'rates' => '',
            'industry' => 'House Hold',
            'products' => 'Dazz',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000326 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '25 X 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Pampangas Best',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000327 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 60',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Suntrust',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000328 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '35 X 50',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'Isumbong mo kay Tulfo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000329 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Grand Monaco',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000330 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'King Sisig',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000331 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Crocs',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000332 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Services',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000333 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Canadian Club',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000334 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '35 X 50',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Hello Kitty Event',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000335 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 30',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'KTC LED TV',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000336 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Prime MOM',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000337 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Goldilucks',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000338 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '24 X 30',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'POS',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000339 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 40',
            'rates' => '',
            'industry' => 'Store',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000340 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '25 X 25',
            'rates' => '',
            'industry' => 'Digital',
            'products' => 'A1 Software',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000341 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000342 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '35 X 50',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Bulalo World',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000343 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '20 X 60',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Sogo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000344 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 40',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'ORGANIQUE',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000345 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 30',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000346 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Pasay',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '10 X 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Cervega Negra',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000347 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 60',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000348 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '10 X 1000',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'Kolin',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000349 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '6 X 35',
            'rates' => '',
            'industry' => 'Appliances',
            'products' => 'Kolin',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000350 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '70 X 50',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Security Bank',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000350 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '70 X 50',
            'rates' => '',
            'industry' => 'Financial',
            'products' => 'Security Bank',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000351 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '100 X 70',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Bench',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000352 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 60',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000353 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 60',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000354 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 20',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Seetrus',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000355 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 20',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Master Hans',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000356 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000357 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '10 X 3',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Honda Beat',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000358 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'MRT Foxy Banners',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '10 X 3',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Tropicana',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000359 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000360 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 20',
            'rates' => '',
            'industry' => 'Event',
            'products' => 'Bryn Adams Concert',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000361 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 20',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'DZBB',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000361 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 20',
            'rates' => '',
            'industry' => 'Services',
            'products' => 'Master Hans',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000363 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Ligo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000364 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Lexus',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000365 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 15',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Century',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000366 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 60',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Century',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000367 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '6 X 15',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000368 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 50',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Sunnies',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000369 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Contis',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000370 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 80',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Don Henricos',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000371 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 60',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'City Mall',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000372 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 60',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'City Mall',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000372 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Shakeys',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000373 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 30',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Shakeys',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000374 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Alveo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000375 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 40',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000376 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 20',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Premium Beers',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000377 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 60',
            'rates' => '',
            'industry' => 'Motoring',
            'products' => 'Suzuki',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000378 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Building Sticker Wrap',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '35 X 70',
            'rates' => '',
            'industry' => 'Food',
            'products' => 'Oishi',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000379 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 60',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Havaianas',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000380 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '150 X 150',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Penshoppe',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000381 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 60',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Facial Care Center',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000382 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 50',
            'rates' => '',
            'industry' => 'Entertainment',
            'products' => 'PBA',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000383 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Building LED Curtain',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '200 X 100',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Wilkins',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000384 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Bus Shed Ads',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '8 X 50',
            'rates' => '',
            'industry' => 'Real Estate',
            'products' => 'Empire East',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000385 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Wrangler',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000386 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 50',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Chowking',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000387 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 40',
            'rates' => '',
            'industry' => 'Restaurant',
            'products' => 'Greenwich',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000388 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '80 X 110',
            'rates' => '',
            'industry' => '',
            'products' => '',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000389 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 60',
            'rates' => '',
            'industry' => 'Store',
            'products' => 'Abensons',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000390 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '40 X 180',
            'rates' => '',
            'industry' => 'Personal Care',
            'products' => 'Olay',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000391 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '35 X 80',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Mountain Dew',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000392 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 80',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Uniqlo',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000393 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'LED Board',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '60 X 80',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Pepsi',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000394 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 40',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Pepsi',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000395 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 15',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Pepsi',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000396 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '50 X 15',
            'rates' => '',
            'industry' => 'Beverage',
            'products' => 'Pepsi',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000397 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Sperry',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();

        $portal = new \App\Portal([
            'user_id' => 1,
            'name' => 'EDSA 0000398 NB',
            'supplier' => '',
            'company' => '',
            //'inventory_image' => '303-guadalupetoroxas.jpg',
            //'category'='Hotel',
            'format' => 'Static Billboard',
            'country' => 'Philippines',
            'city' => 'Makati',
            'street_address' => 'ROXAS BLVD TO GUADALUPE',
            'size' => '30 X 40',
            'rates' => '',
            'industry' => 'Apparel',
            'products' => 'Merrel',
            'age' => '',
            'economic_class' => '',
            'latitude' => 14.543361,
            'longitude' => 121.021803,
            ]);
        $portal->save();
        */
        
    }
}
