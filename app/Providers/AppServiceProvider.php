<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.user-dashboard-layout', function($view) {
            $users = \App\User::all();
            $view->with(compact('users'));
        });

        view()->composer('includes.user-dashboard-nav', function($view) {
            $notifications = Auth::user()->Notifications;
            $notification_count = Auth::user()->unreadNotifications()->count();
            $view->with(compact('notifications', 'notification_count'));
        });

        view()->composer('includes.user-dashboard-sidebar', function($view) {
            $notification_count = Auth::user()->unreadNotifications->count();
            $view->with(compact('notification_count'));
        });

        view()->composer('includes.quotes-modal', function($view) {
            $materialchanges = \App\materialchanges::all();
            $productions = \App\production::all();
            $view->with(compact('materialchanges', 'productions'));
        });

        view()->composer('includes.request-demo-modal', function($view) {
            $demooptions = \App\DemoOption::all();
            $view->with(compact('demooptions'));
        });

        view()->composer('*', function ($view) {
            $current_route_name = \Request::route()->getName();
            $view->with('current_route_name', $current_route_name);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
