<?php

namespace App\Listeners;

use App\Events\BookingMemoDeleteEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\booking;
use Mail;

class BookingMemoDeleteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingMemoDeleteEvent  $event
     * @return void
     */
    public function handle(BookingMemoDeleteEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $book = booking::find($event->bookId)->toArray();

        Mail::send('emails.booking-memo-delete-email-user', compact('user', 'book'), function ($message) use ($book) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($book['user_email'])->subject('Booking Deleted');
        });

        Mail::send('emails.booking-memo-delete-email-receiver', compact('user', 'book'), function ($message) use ($book) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($book['receiver_email'])->subject('Booking Deleted');
        });
    }
}
