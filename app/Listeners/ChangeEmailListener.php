<?php

namespace App\Listeners;

use App\Events\ChangeEmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class ChangeEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChangeEmailEvent  $event
     * @return void
     */
    public function handle(ChangeEmailEvent $event)
    {
        $user = User::find($event->userId)->toArray();

        Mail::send('emails.user-profile-change-email-email', compact('user'), function ($message) use ($user) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($user['email'])->subject('Change Email');
        });
    }
}
