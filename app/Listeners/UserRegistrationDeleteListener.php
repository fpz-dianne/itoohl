<?php

namespace App\Listeners;

use App\Events\UserRegistrationDeleteEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class UserRegistrationDeleteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistrationDeleteEvent  $event
     * @return void
     */
    public function handle(UserRegistrationDeleteEvent $event)
    {
        $user = User::find($event->userId)->toArray();

        Mail::send('emails.user-registration-delete', compact('user'), function ($message) use ($user) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($user['email'])->subject('Account Deleted');
        });
    }
}
