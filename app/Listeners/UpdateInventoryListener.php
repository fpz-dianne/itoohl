<?php

namespace App\Listeners;

use App\Events\UpdateInventoryEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Portal;
use Mail;

class UpdateInventoryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateInventoryEvent  $event
     * @return void
     */
    public function handle(UpdateInventoryEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $portal = Portal::find($event->portalId)->toArray();

        Mail::send('emails.inventory-update-email', compact('user', 'portal'), function ($message) use ($user) {
                $message->from( env('MAIL_USERNAME') );
                $message->to($user['email'])->subject('Inventory Updated');
        });
    }
}
