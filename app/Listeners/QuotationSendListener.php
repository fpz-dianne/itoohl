<?php

namespace App\Listeners;

use App\Events\QuotationSendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\RequestQuotation;
use Mail;

class QuotationSendListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuotationSendEvent  $event
     * @return void
     */
    public function handle(QuotationSendEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $data = $event->data;
        $quote = RequestQuotation::find($event->request_quoteId)->toArray();

        Mail::send('emails.quotation-send-email', compact('user', 'data', 'quote'), function ($message) use ($quote) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($quote['user_email'])->subject('Send Quotations');
        });
    }
}
