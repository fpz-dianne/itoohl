<?php

namespace App\Listeners;

use App\Events\QuotationRequestEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class QuotationRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuotationRequestEvent  $event
     * @return void
     */
    public function handle(QuotationRequestEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $receiver_email = $event->receiver_email;
        $data = $event->data;

        Mail::send('emails.quotation-request-email', compact('user', 'data'), function ($message) use ($receiver_email) {
                $message->from( env('MAIL_USERNAME') );
                $message->to($receiver_email)->subject('Request Quotations');
            });
    }
}
