<?php

namespace App\Listeners;

use App\Events\UserChangeRoleEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class UserChangeRoleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserChangeRoleEvent  $event
     * @return void
     */
    public function handle(UserChangeRoleEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $role = $event->role;

        Mail::send('emails.user-registration-change-role', compact('user', 'role'), function ($message) use($user) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($user['email'])->subject('Change Role');
        });
    }
}
