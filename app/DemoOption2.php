<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoOption2 extends Model {

    protected $fillable = array('name');

	public function requestDemos2() {
        return $this->hasMany('App\requestdemo');
    }
}
