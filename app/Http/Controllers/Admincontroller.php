<?php

namespace App\Http\Controllers;


use Image;
use Mail;

use App\admin;
use App\Portal;
use App\country;
use App\city;
use App\industry;
use App\product;
use App\size;
use App\format;
use App\payment_term;
use Session;
use Alert;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;


class Admincontroller extends Controller {

	public function getLogin() {

		if (Auth::check()) {
		    return redirect()->route('user-dashboard');
		}
		
		return view('admin.admin-login');
	}

	public function postAdminLogin(Request $request) {

		$this->validate($request, [
			'email' => 'required',
			'password' => 'required'
		]);

		if (Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => 1])) {

			alert()->success('Login Succesful');
			
			return redirect()->route('user-dashboard');
		}
		elseif(Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => 2], false, false)) {

			alert()->warning('Account need to verify')->persistent('Close');

			return redirect()->back()->withInput();
		}
		else {
			alert()->warning('Email or password is wrong')->persistent('Close');
		}

		return redirect()->route('login')->withInput();
	}

	public function getAdminLogout() {
		Auth::logout();
		Session::flush();
		return redirect()->route('login');
	}

}
