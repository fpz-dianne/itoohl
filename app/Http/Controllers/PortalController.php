<?php

namespace App\Http\Controllers;

	use App\Portal;
	use App\industry;
	use App\availability;
	use App\format;
	use App\gender;
	use App\age;
	use App\EconomicClass;
	use App\city;
	use App\materialchanges;
	use App\profile;

	use Session;
	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Input;

class PortalController extends Controller
{

	//Map View of portal
    public function getPortal() {

		//$portals = Portal::orderBy('id')->get();

		//return view('portal', ['portals' => $portals]);

		$industries = Industry::all();
		$availabilities = availability::all();
		$formats = Format::all();
		$cities = City::all();
		$genders = Gender::all();
		$classes = EconomicClass::all();
		$ages = Age::all();
		$profiles = Profile::all();

		return view('portal', compact('industries', 'availabilities', 'formats', 'genders', 'ages', 'classes' , 'cities', 'profiles'));
	}


	//List view of portal
	public function getPortalListView() {

		$portals = Portal::orderBy('id')->get();

		return view('portal-list', ['portals' => $portals]);
	}

	public function getAjaxFilterInventory() {

		$filter = Input::get('filter_id');
		
		$inventory = Portal::where(function ($query) use ($filter) {
	            $query->orwhere('industry', '=', isset($filter) ? $filter : null)
                  ->orwhere('format', '=', isset($filter) ? $filter : null)
                  ->orwhere('availability', '=', isset($filter) ? $filter : null)
                  ->orwhere('gender', '=', isset($filter) ? $filter : null)
                  ->orwhere('economic_class', '=', isset($filter) ? $filter : null)
                  ->orwhere('age', '=', isset($filter) ? $filter : null)
                  ->orwhere('profile', '=', isset($filter) ? $filter : null)
                  ->orwhere('economic_class', '=', isset($filter) ? $filter : null)
                  ->orwhere('city', '=', isset($filter) ? $filter : null);
	        })
			->with('user')
			->get();


	    return response()->json($inventory);
	}

	public function getAjaxFilterTraffic() {
		$inventory = Portal::with('user')->get();

		return response()->json($inventory);
	}

	public function getAjaxFilterCircle() {
		$inventory = Portal::with('user')->get();

		return response()->json($inventory);
	}

	public function getAjaxAutocomplete() {
		$search = Input::get('term');

		$results = array();
	
		$portals = Portal::where('name', 'like', '%'.$search.'%')->get();
		$industries = Industry::where('name', 'like', '%'.$search.'%')->get();
		$formats = Format::where('name', 'like', '%'.$search.'%')->get();
		$cities = City::where('name', 'like', '%'.$search.'%')->get();
		$profiles = Profile::where('name', 'like', '%'.$search.'%')->get();
		
		if ($portals->count()) {
			foreach ($portals as $portal) {
			    $results[] = [ 'id' => $portal->id, 'value' => $portal->name];
			}
		}
		else if ($industries->count()) {
			foreach ($industries as $industry) {
			    $results[] = [ 'id' => $industry->id, 'value' => $industry->name];
			}
		}
		else if ($formats->count()) {
			foreach ($formats as $format) {
			    $results[] = [ 'id' => $format->id, 'value' => $format->name];
			}
		}
		else if ($cities->count()) {
			foreach ($cities as $city) {
			    $results[] = [ 'id' => $city->id, 'value' => $city->name];
			}
		}
		else if ($profiles->count()) {
			foreach ($profiles as $profile) {
			    $results[] = [ 'id' => $profile->id, 'value' => $profile->name];
			}
		}


		return response()->json($results);
	}

	public function getAjaxSearch() {

		$search = Input::get('search_id');
		
		$portals = Portal::where('name', 'like', '%'.$search.'%')->with('user')->get();
		$industries = Portal::where('industry', 'like', '%'.$search.'%')->with('user')->get();
		$formats = Portal::where('format', 'like', '%'.$search.'%')->with('user')->get();
		$cities = Portal::where('city', 'like', '%'.$search.'%')->with('user')->get();
		$profiles = Portal::where('profile', 'like', '%'.$search.'%')->with('user')->get();
		
		if ($portals->count()) {
			    $result = $portals;
		}
		else if ($industries->count()) {
			    $result = $industries;
		}
		else if ($formats->count()) {
			    $result = $formats;
		}
		else if ($cities->count()) {
			    $result = $cities;
		}
		else if ($profiles->count()) {
			    $result = $profiles;
		}

	    return response()->json($result);
	}

	public function getAjaxAutocompleteSelect() {

		$data = Input::get('data_id');
		
		$portals = Portal::where('name', 'like', '%'.$data.'%')->with('user')->get();
		$industries = Portal::where('industry', 'like', '%'.$data.'%')->with('user')->get();
		$formats = Portal::where('format', 'like', '%'.$data.'%')->with('user')->get();
		$cities = Portal::where('city', 'like', '%'.$data.'%')->with('user')->get();
		$profiles = Portal::where('profile', 'like', '%'.$data.'%')->with('user')->get();
		
		if ($portals->count()) {
			    $result = $portals;
		}
		else if ($industries->count()) {
			    $result = $industries;
		}
		else if ($formats->count()) {
			    $result = $formats;
		}
		else if ($cities->count()) {
			    $result = $cities;
		}
		else if ($profiles->count()) {
			    $result = $profiles;
		}

	    return response()->json($result);
	}

	public function getAjaxBookmarkView($id) {

		$industries = Industry::all();
		$availabilities = availability::all();
		$formats = Format::all();
		$cities = City::all();
		$genders = Gender::all();
		$classes = EconomicClass::all();
		$ages = Age::all();
		$profiles = Profile::all();
		
		$inventory = Portal::where('id', $id)
			->with('user')
			->get();

		//dd($inventory);

	    return redirect()->route('portal');
	}

}
