<?php

namespace App\Http\Controllers;

	use Mail;
	use Session;
	use Alert;

	use App\User;
	use App\deactivateacc;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;

class DeactivateAccntController extends Controller
{
   public function postDeactivateAccnt(Request $request ) {
   		$reason = $request['reason'];
   		$deactivate_accnt = new Deactivateacc();
   		$deactivate_accnt->reason = $reason;
   		$request->user()->deactivateacc()->save($deactivate_accnt);
   		alert()->success('Request Submitted')->persistent('Confirm');

   		return redirect()->route('profile');
   }
}
