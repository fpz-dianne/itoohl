<?php

namespace App\Http\Controllers;

	use Mail;
	use Session;
	use Alert;

	use App\User;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;

class NewsController extends Controller {
    
    //single news
    public function getSingleNews() {
    	
    	return view('news.single-news');

    }
}
