<?php

namespace App\Http\Controllers;

	use Mail;
	use Alert;
    use Carbon;

	use App\User;
    use App\requestdemo;
    use App\DemoOption;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;
    use Illuminate\Notifications\Notifiable;

    use App\Notifications\RequestDemoNotification;


class RequestDemoController extends Controller {

    //send request demo
    public function postRequestDemo(Request $request) {
    	
        $name = $request['name'];
    	$email = $request['email'];
        $company = $request['company'];
        $option = $request['option'];

    	$request_demo = new requestdemo();
        $request_demo->DemoOption_id = $option[0];
        $request_demo->DemoOption2_id = isset($option[1]) ? $option[1] : $option[0];
    	$request_demo->name = $name;
        $request_demo->email = $email;
        $request_demo->company = $company;

        $request_demo->save();

        //Notify
        User::where('id', 1)->first()->notify(new RequestDemoNotification($name));
		
        //use admin id to send email
		$user = User::where('id', 1)->get();

    	alert()->success('Thank you for your interest in iTOOhL. We will get in contact with you as soon as possible.')->persistent('Confirm');
		
		Mail::send('emails.demo-request-admin-email', ['user' => $user, 'demo' => $request_demo ], function ($message) {
			$message->from( env('MAIL_USERNAME') );
			$message->to(env('MAIL_USERNAME'))->subject('Request Demo');
		});
		
		Mail::send('emails.demo-request-user-email', ['user' => $user, 'demo' => $request_demo ], function ($message) use ($request) {
			$message->from( env('MAIL_USERNAME') );
			$message->to($request['email'])->subject('Request Demo');
		});

    	return redirect()->route('home');

    }

    //request demo list
    public function getDemoList() {
        $demos = Requestdemo::all();
        $demooptions = DemoOption::all();
        
        //$user = Auth::user();
        
        //$notification = $user->notifications()->where('type', 'App\Notifications\RequestDemoNotification')->get();
            
        //$notification->markAsRead();
        return view('user.demo-list', compact('demos', 'demooptions'));
    }

    //request demo Delete
    public function getDemoDelete($id) {

        $demo = Requestdemo::findOrFail($id);

        $demo->delete();

        alert()->success('Record Deleted');

        return redirect()->route('demo-list');
    }

    //Send demo
    public function getSendDemo($id) {
        dd('sdss');
    }
}
