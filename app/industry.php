<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class industry extends Model
{
    protected $fillable = array('name');
}
