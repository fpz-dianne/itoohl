<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdateInventoryEvent extends Event
{
    use SerializesModels;

    public $userId;
    public $portalId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $portalId)
    {
        $this->userId = $userId;
        $this->portalId = $portalId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
