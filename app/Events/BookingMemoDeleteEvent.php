<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BookingMemoDeleteEvent extends Event
{
    use SerializesModels;

    public $userId;
    public $bookId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $bookId)
    {
        $this->userId = $userId;
        $this->bookId = $bookId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
