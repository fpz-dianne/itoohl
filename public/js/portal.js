
jQuery(document).ready(function($){
	//open/close lateral filter
	$('.filter-trigger').on('click', function(){
		triggerFilter(true);
	});
	$('.filter .close').on('click', function(){
		triggerFilter(false);
	});

	function triggerFilter($bool) {
		var elementsToTrigger = $([$('.filter-trigger'), $('.filter'), $('.tab-filter')]);
		elementsToTrigger.each(function(){
			$(this).toggleClass('filter-is-visible', $bool);
		});
	}

	//mobile version - detect click event on filters tab
	var filter_tab_placeholder = $('.tab-filter .placeholder a'),
		filter_tab_placeholder_default_value = 'Select',
		filter_tab_placeholder_text = filter_tab_placeholder.text();
		$('.filter-block h4').addClass('closed');
	
	$('.tab-filter li').on('click', function(event){
		//detect which tab filter item was selected
		var selected_filter = $(event.target).data('type');
			
		//check if user has clicked the placeholder item
		if( $(event.target).is(filter_tab_placeholder) ) {
			(filter_tab_placeholder_default_value == filter_tab_placeholder.text()) ? filter_tab_placeholder.text(filter_tab_placeholder_text) : filter_tab_placeholder.text(filter_tab_placeholder_default_value) ;
			$('.tab-filter').toggleClass('is-open');

		//check if user has clicked a filter already selected 
		} else if( filter_tab_placeholder.data('type') == selected_filter ) {
			filter_tab_placeholder.text($(event.target).text());
			$('.tab-filter').removeClass('is-open');	

		} else {
			//close the dropdown and change placeholder text/data-type value
			$('.tab-filter').removeClass('is-open');
			filter_tab_placeholder.text($(event.target).text()).data('type', selected_filter);
			filter_tab_placeholder_text = $(event.target).text();
			
			//add class selected to the selected filter item
			$('.tab-filter .selected').removeClass('selected');
			$(event.target).addClass('selected');
		}
	});
	
	//close filter dropdown inside lateral .filter 
	$('.filter-block h4').on('click', function(){
		$(this).toggleClass('closed').siblings('.filter-content').slideToggle(300);
	})

});



jQuery(document).ready(function($){
	//open/close lateral filter

	$('.inventory .close').on('click', function(){
		triggerInventory(false);
	});

	function triggerInventory($bool) {
		var elementsToTrigger = $([$('.inventory')]);
		elementsToTrigger.each(function(){
			$(this).toggleClass('inventory-is-visible', $bool);
		});
	}

	//Slim scroll
	$(function(){
	    $('.side-bar-container').slimScroll({
	    	railVisible: true,
    		railColor: '#222',
    		railBorderRadius: '0',
    		borderRadius: '0',
    		height: '300px',
	        width: '100%',
    });
	    $('.filter-scroll').slimScroll({
	    	railVisible: true,
    		railColor: '#222',
    		railBorderRadius: '0',
    		borderRadius: '0',
    		height: '220px',
	        width: '100%'
    });
});

});

//tooltip
$(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
});

//switch
$(document).ready(function() {
	$('#demographic').click(function() {

		//Uncheck all filter
		$('#demographic').change(function() {	
			$('.filter input[type=checkbox]').each(function () {
	           if (this.checked) {
	               $(this).trigger('click'); 
	           }
			});
		});	


		$('.demographic-filter').removeClass('is-hidden');
	    $('.basic-filter').addClass('is-hidden');
	});

	$('#basic').click(function() {
		
		//Uncheck all filter
		$('#basic').change(function() {	
			$('.filter input[type=checkbox]').each(function () {
	           if (this.checked) {
	               $(this).trigger('click'); 
	           }
			});
		});	

		$('.basic-filter').removeClass('is-hidden');
	    $('.demographic-filter').addClass('is-hidden');
	});
});

//autocomplete search
$(function() {
	 $( "#search" ).autocomplete({
	  source: "ajax-autocomplete-industry",
	  minLength: 3,
	  select: function(event, data) {
	  	$('#search').val(data.item.value);
	  }
	});
});


$(function() {
	 $( "#search" ).autocomplete({
	  source: "ajax-autocomplete",
	  minLength: 3,
	  select: function(event, data) {
	  	$('#search').val(data.item.value);
	  }
	});
});





  