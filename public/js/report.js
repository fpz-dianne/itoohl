$(function () {

  'use strict';

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });

  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");
  var pieChart1 = new Chart(pieChartCanvas1);

  var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
  var pieChart2 = new Chart(pieChartCanvas2);
  var PieData = [
    {
      value: 13,
      color: "#f56954",
      highlight: "#f56954",
      label: "Apparel"
    },
    {
      value: 7,
      color: "#00a65a",
      highlight: "#00a65a",
      label: "Entertainment"
    },
    {
      value: 4,
      color: "#f39c12",
      highlight: "#f39c12",
      label: "Food"
    },
    {
      value: 4,
      color: "#00c0ef",
      highlight: "#00c0ef",
      label: "Personal Care"
    },
    {
      value: 5,
      color: "#3c8dbc",
      highlight: "#3c8dbc",
      label: "Malls"
    },
    {
      value: 1,
      color: "#d2d6de",
      highlight: "#d2d6de",
      label: "Others"
    },
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
    //String - A tooltip template
    tooltipTemplate: "<%=value %> <%=label%> Inventory"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart1.Doughnut(PieData, pieOptions);
  pieChart2.Doughnut(PieData, pieOptions);
  //-----------------
  //- END PIE CHART -
  //-----------------
});






